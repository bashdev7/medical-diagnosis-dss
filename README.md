# Diagnostica
> Medical Diagnosis Decision Support System

## Introduction

Frame-based expert systems are widely used as the knowledge representation for expert systems with large knowledge base. Many systems have the ability to connect to external databases. Facts stored in databases can be loaded into expert system’s knowledge base and inference is performed by the inference engine of the expert system. In many cases, such external facts are required several times for each inference. Thus, a lot of communication traffic takes place. This research work present the design and implementation of a frame-based object-relational database system which has a tight coupling between the expert system and the external knowledge base. The external knowledge base also use frame as its knowledge representation. Moreover, it has its own inference engine so that inference can be perform on the knowledge base side and the results, not only simple facts, are sent back to the expert system for further inference.

Frames are widely used as the knowledge representation of large, complex expert systems However, most such expert system shells have internal frame-based knowledge bases. They are internal in the sense that frames are loaded and stored in the main memory of the expert system during consultation sessions. The knowledge bases do not have the advanced data management facilities such as indexing, query optimization, concurrency control and recovery control which are common in modern database management systems (DBMS). During a consulting session, there are facts that are obtained from the user interactively and facts that are obtained from inferences. Inference rules in conventional expert systems are executed by the inference engine on the expert system’s machine. It mainly uses facts obtained interactively from users.
Facts from external databases are sometimes loaded into the knowledge base when required by the inference process. In simple systems, relations that contain both relevant and irrelevant facts are loaded into the knowledge base. In more advanced systems only related facts are loaded. In both cases, there are no inferences on the external database (or knowledge base) side. Our approach is different. We propose an architecture that includes an inference engine on the external knowledge base side as well as one on the expert system shell side. This approach enables inferences to be performed on the external knowledge base side so that only the inference results are sent back to the expert system instead of sending facts several times during an inference process performed by the expert system’s inference engine.

This proposed study has a unique position in information systems. Involving the organization as a whole, they tend to become the unique nervous system allowing the organization not only to work better or properly, but to work at all. While the concepts of “paperless hospitals” or “digital hospitals” and strategic objectives are increasingly achieved, dependence on these systems is also becoming increasingly important. This dependence takes different faces, at different paces, and most of them don’t have direct solutions that can fix the points easily and definitively. The two major challenges are to create awareness of this dependence, and to have sufficient resources to introduce acceptable answers on one side, and to adapt over time, sustained with the evolution of the systems and the organization on the other side.

## Getting Started
- Content coming soon.

## Setup
- The project uses react-static with firebase

## Deployment
- Coming soon.
