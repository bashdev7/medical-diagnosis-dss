import React, { Component } from 'react'
import { Form, Header, Segment } from 'semantic-ui-react'
import localforage from 'localforage'
import FooterMenu from '../Subpages/FooterMenu'

class HistoryPage extends Component {
  constructor (props) {
    super(props)
    this.state = {}
  }

  handleChange = (e, { name, checked }) => {
    this.setState({ [name]: checked })
  }

  saveHistory () {
    const data = this.state
    localforage.setItem('diagnostica_client_history', JSON.stringify(data))
  }

  componentDidUpdate () {
    this.saveHistory()
  }

  componentWillMount () {
    const app = this
    localforage.getItem('diagnostica_client_history', (e, v) => {
      const data = JSON.parse(v)
      if (!e) {
        app.setState(Object.assign({}, data))
      }
    })
  }

  render () {
    const {
      db,
      sm,
      pi,
      ckd,
      ehd,
      ud,
      hua,
      rs,
      sa,
      ee,
    } = this.state
    return (
      <div>
        <Segment padded>
          <Header as="h4" content="Past or current conditions" />
          <Form>
            <Form.Checkbox name="db" checked={db} onChange={this.handleChange} label="Diabetes" />
            <Form.Checkbox name="sm" checked={sm} onChange={this.handleChange} label="Smoking" />
            <Form.Checkbox name="pi" checked={pi} onChange={this.handleChange} label="Physically Inactive" />
            <Form.Checkbox name="ckd" checked={ckd} onChange={this.handleChange} label="Chronic Kidney Disease" />
            <Form.Checkbox name="ehd" checked={ehd} onChange={this.handleChange} label="Family History of Early Heart Disease" />
            <Form.Checkbox name="ud" checked={ud} onChange={this.handleChange} label="Unhealthy Diet" />
            <Form.Checkbox name="hua" checked={hua} onChange={this.handleChange} label="Heavy Use of Alchohol" />
            <Form.Checkbox name="rs" checked={rs} onChange={this.handleChange} label="Regular Stress" />
            <Form.Checkbox name="sa" checked={sa} onChange={this.handleChange} label="Sleep Apnea" />
            <Form.Checkbox name="ee" checked={ee} onChange={this.handleChange} label="Excessive Eating" />
          </Form>
        </Segment>
        <FooterMenu previous="/symptoms/" previousDisabled={false} next="/conditions/" nextDisabled={false} />
      </div>
    )
  }
}

export default HistoryPage
