import React, { Component } from 'react'
import { Link } from 'react-static'
import { Button, Message, Segment } from 'semantic-ui-react'

class TreatmentPage extends Component {
  render () {
    return (
      <div>
        <Segment padded>
          <Message>
            Needs Research
          </Message>
          <Button color="blue" size="large" fluid content="Try Again" as={Link} to="/" />
        </Segment>
      </div>
    )
  }
}

export default TreatmentPage
