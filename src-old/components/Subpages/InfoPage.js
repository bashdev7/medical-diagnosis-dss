import localforage from 'localforage'
import React, { Component } from 'react'
import { Dropdown, Grid, Header, Input, Message, Segment } from 'semantic-ui-react'
import FooterMenu from '../Subpages/FooterMenu'

const genderOptions = [
  { text: 'Male', value: 1 },
  { text: 'Female', value: 0 },
]

class InfoPage extends Component {
  constructor (props) {
    super(props)
    const app = this
    app.state = {}
  }

  saveInfo () {
    const data = this.state
    localforage.setItem('diagnostica_client', JSON.stringify(data))
  }
    handleChange = (e, { name, value }) => {
      console.log({ [name]: value })
      this.setState({ [name]: value })
    }

    componentDidUpdate () {
      this.saveInfo()
    }

    componentDidMount () {
      const app = this
      localforage.getItem('diagnostica_client', (e, v) => {
        const data = JSON.parse(v)
        if (!e) {
          app.setState(Object.assign({}, data))
        }
      })
    }

    render () {
      const {
        age,
        gender,
        height,
        weight,
        bps,
        bpd,
      } = this.state

      const isCleared = (age!== undefined && age!== 0) &&
      (gender!== undefined && gender!== null) &&
      (height!== undefined && height!== 0) &&
      (weight!== undefined && weight!== 0) &&
      (bps!== undefined && bps!== 0) &&
      (bpd!== undefined && bpd!== 0)
      return (
        <div>
          <Message header="Live update support!" color="blue" content="Your changes are automatically saved." />
          <Segment padded>
            <Grid>
              <Grid.Row columns={2}>
                <Grid.Column>
                  <Header as="h4" content="Age" />
                  <Input name="age" value={age || 0} onChange={this.handleChange} type="number" fluid />
                  <Header as="h4" content="Gender" />
                  <Dropdown placeholder="Select Gender" name="gender" value={gender} onChange={this.handleChange} fluid search selection options={genderOptions} />
                  <Header as="h4" content="Height" />
                  <Input type="number" name="height" value={height || 0} onChange={this.handleChange} fluid />
                  <Header as="h4" content="Weight (kg)" />
                  <Input type="number" name="weight" value={weight || 0} onChange={this.handleChange} fluid />
                  <Header as="h4" content="Blood Pressure (mmHg)" />
                  <Input label="Systolic" name="bps" value={bps || 0} onChange={this.handleChange} type="number" fluid /><br />
                  <Input label="Diastolic" type="number" name="bpd" value={bpd || 0} onChange={this.handleChange} fluid />
                </Grid.Column>
                <Grid.Column>
                  <Header content="Disclaimer" />
                  <p>
                    Diagnostica is only intended for informational purposes only and diagnosis/conditions results should be taken with a grain of salt. Still it is recommended to seek for a professional medical advice, diagnosis or treatment.
                  </p>
                </Grid.Column>
              </Grid.Row>
            </Grid>
            <br />
          </Segment>
          <FooterMenu previous="" previousDisabled next="/symptoms/" nextDisabled={!isCleared} />
        </div>
      )
    }
}

export default InfoPage
