import _ from 'lodash'
import React, { Component } from 'react'
import { Button, Divider, Dropdown, Grid, Header, Segment, Message } from 'semantic-ui-react'
import localforage from 'localforage'

import FooterMenu from '../Subpages/FooterMenu'
import { symptoms as symptomOptions } from '../../config/symptoms'

class SymptomsPage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      symptoms: [],
    }
  }

    handleRemoveSymptom = key => {
      const { symptoms } = this.state

      _.remove(symptoms, n => n === key)

      this.setState(symptoms)
    }

    saveInfo () {
      const data = this.state
      localforage.setItem('diagnostica_client_symptoms', JSON.stringify(data))
    }

    handleChange = (e, { name, value }) => {
      const { symptoms } = this.state
      this.setState({ [name]: value })
      symptoms.push(value)
      this.setState({ symptoms: _.uniq(symptoms) })
    }

    componentDidUpdate () {
      this.saveInfo()
    }

    loadData () {
      const app = this
      localforage.getItem('diagnostica_client_symptoms', (e, v) => {
        const data = JSON.parse(v)
        if (!e) {
          console.log(data)
          app.setState(Object.assign(app.state, data))
        }
      })

      localforage.getItem('diagnostica_client', (e, v) => {
        const data = JSON.parse(v)
        if (!e) {
          app.setState(Object.assign(app.state, data))
        }
      })
    }

    componentWillMount () {
      this.loadData()
    }

    render () {
      const {
        symptoms,
        age,
        gender,
        weight,
        height,
        bps,
        bpd,
      } = this.state

      // BMI

      let bmiCalc = 0
      let bmiState = 'Healthy'

      if (weight!==0 && height !== 0) {
        bmiCalc = Math.round((weight / height**2)*100) / 100
      }

      if (bmiCalc >= 25.0) {
        bmiState = 'Overweight'
      }

      if (bmiCalc >= 18.5 && bmiCalc <= 24.9) {
        bmiState = 'Healthy'
      }

      if (bmiCalc < 18.5) {
        bmiState = 'Underweight'
      }

      // Blood Pressure
      let bpState = 'Normal'

      if ((bps <= 90 && bps >= 70) && (bpd <= 60 && bpd >= 40)) {
        bpState = 'Low'
      }

      if ((bps <= 120 && bps >= 91) && (bpd <= 80 && bpd >= 61)) {
        bpState = 'Normal'
      }

      if ((bps <= 140 && bps >= 121) && (bpd <= 90 && bpd >= 81)) {
        bpState = 'Pre Hypertension'
      }

      if ((bps <= 160 && bps >= 141) && (bpd <= 100 && bpd >= 90)) {
        bpState = 'High: Stage 1 Hypertension'
      }

      if ((bps >= 161) && (bpd >= 101)) {
        bpState = 'High: Stage 2 Hypertension'
      }


      return (
        <div>
          <Segment padded>
            <Grid>
              <Grid.Column width={10}>
                <Header as="h4" content="What symptoms have you observed in yourself?" />
                <p>
                  <span>A symptom is any subjective evidence of disease, </span>
                  <span>while a sign is any objective evidence of disease.</span>
                  <span>Therefore, a symptom is a phenomenon that is experienced by</span>
                  <span>the individual affected by the disease, while a sign is a phenomenon </span>
                  <span>that can be detected by</span>
                  <span>someone other than the individual affected by the disease.</span>
                </p>
                <Dropdown onChange={this.handleChange} name="symptom" placeholder="Add a Symptom" search selection fluid options={symptomOptions} />
                {symptoms.length !== 0 && <Divider />}
                <Grid stackable columns={2}>
                  {symptoms !== [] && symptoms.map(i => (
                    <Grid.Column key={i}>
                      <Button onClick={() => this.handleRemoveSymptom(i)} fluid size="medium" icon="delete" labelPosition="right" content={symptomOptions[i].text} color="blue" />
                    </Grid.Column>
                                ))}
                </Grid>
                <br />
              </Grid.Column>
              <Grid.Column width={6}>
                <Message>
                  <Message.Header>Patient Info</Message.Header>
                  <Divider />
                  <Message.Content>
                    <p><b>Age:</b> {age || 0}</p>
                    <p><b>Gender:</b> {gender?'Male':'Female'}</p>
                    <p><b>Body Mass Index:</b>  {bmiCalc}</p>
                    <p><b>Body Mass Index State:</b>  {bmiState}</p>
                    <p><b>Blood Pressure:</b>  {bpState}</p>
                  </Message.Content>
                </Message>

              </Grid.Column>
            </Grid>
          </Segment>
          <FooterMenu previous="" previousDisabled={false} next="/history/" nextDisabled={false} />
        </div>
      )
    }
}

export default SymptomsPage
