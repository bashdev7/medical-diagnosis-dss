import React, { Component } from 'react'
import { ArrowLeft, ArrowRight } from 'react-feather'
import { Link } from 'react-static'
import { Icon, Menu } from 'semantic-ui-react'


class FooterMenu extends Component {
  render () {
    return (
      <Menu>
        <Menu.Item as={Link} disabled={this.props.previousDisabled} to={this.props.previous}>
          <Icon as={ArrowLeft} /> Previous
        </Menu.Item>
        <Menu.Menu position="right">
          <Menu.Item as={Link} disabled={this.props.nextDisabled} to={this.props.next}>
              Next <Icon as={ArrowRight} />
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    )
  }
}


export default FooterMenu
