import React, { Component } from 'react'
import { Link, Redirect } from 'react-static'
import axios from 'axios'
import { Button, Card, Grid, Header, Segment, Progress } from 'semantic-ui-react'
import _ from 'lodash'
import localforage from 'localforage'
import { symptoms } from '../../config/symptoms'

import FooterMenu from '../Subpages/FooterMenu'

class ConditionsPage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      loading: true,
      redirect: false,
    }
  }

  loadData () {
    const app = this
    let history = []
    const symptomList = []

    localforage.getItem('diagnostica_client_symptoms', (e, v) => {
      const data = JSON.parse(v)
      if (!e) {
        const list = data.symptoms
        list.map(i => {
          symptomList.push(_.toLower(symptoms[i].text))
        })
      }
    })

    localforage.getItem('diagnostica_client_history', (e, v) => {
      const data = JSON.parse(v)
      if (!e) {
        const keys = Object.keys(data)
        keys.map(i => {
          if (!data[i]) {
            _.remove(keys, n => data[n] === false)
          }
        })
        history = keys
      }
    })

    localforage.getItem('diagnostica_client', (e, v) => {
      const data = JSON.parse(v)
      if (!e) {
        app.setState({ client: data })
        const factors = [].concat.apply([], symptomList, [])
        axios.get('https://diagnostica-api.herokuapp.com/diagnosis', {
          params: {
            symptoms: factors,
          },
        }).then(res => {
          this.setState({ conditions: res.data.conditions, loading: false })
        })
      }
    })
  }

  componentWillMount () {
    this.loadData()
  }

  getDetails = disease => {
    console.log(disease)
    this.setState({ redirect: true })
  }

  componentDidMount () {

    // const symptomList = []
    // symptoms.map(i => {
    //   symptomList.push(symptoms[i])
    // })
  }
  
  render () {
    const { conditions, loading, redirect } = this.state
    if (redirect) return <Redirect to="/details/" />
    return (
      <div>
        <Segment padded>
          <Header as="h4" content="Findings" />
          <p>The system found the following diseases based on the data you entered.</p>
          <Segment basic loading={loading}>
            <Grid stackable columns={3}>
              {conditions && Object.keys(conditions).map(i => (
                <Grid.Column key={i}>
                  <Card color={i==0?'green':'orange'}>
                    {i==0&&<Progress color="green" percent={100} attached="top" />}
                    <Card.Content>
                      <Card.Header>{_.startCase(_.camelCase(conditions[i].label))}</Card.Header>
                      <Card.Description>
                        Matched Disease
                      </Card.Description>
                      <Card.Description>
                        <small><code>Score: {conditions[i].value}</code></small>
                      </Card.Description>
                    </Card.Content>
                    <Card.Content extra>
                      <Button icon="search" fluid color={i==0?'green':'grey'} content="Lookup" onClick={()=>this.getDetails(conditions[i].label)} />
                    </Card.Content>
                  </Card>
                </Grid.Column>
              ))}
            </Grid>
          </Segment>
        </Segment>
        <FooterMenu previous="/history/" previousDisabled={false} next="/treatment/" nextDisabled />
      </div>
    )
  }
}

export default ConditionsPage
