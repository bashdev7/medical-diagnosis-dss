import React, { Component } from 'react'
import { Activity, CheckSquare, Clock, Heart, HelpCircle, Info, List } from 'react-feather'
import { Head, NavLink, Route, Router, Switch, withSiteData } from 'react-static'
import { Container, Header, Icon, Menu, Progress, Segment, Step } from 'semantic-ui-react'

import logoImg from '../assets/logo.svg'
import noise from '../assets/noise.png'
import socialImg from '../assets/social-img.png'
import InfoPage from '../components/Subpages/InfoPage'
import SymptomsPage from '../components/Subpages/SymptomsPage'
import HistoryPage from '../components/Subpages/HistoryPage'
import ConditionsPage from '../components/Subpages/ConditionsPage'
import DetailsPage from '../components/Subpages/DetailsPage'
import TreatmentPage from '../components/Subpages/TreatmentPage'

class Home extends Component {
  constructor (props) {
    super(props)
    this.state = {}
  }

  render () {
    return (
      <Container className="full-page-container" fluid style={{ backgroundImage: `url(${noise})` }}>
        <Head>
          <title>Diagnostica</title>
          <meta property="og:url" content="https://diagnostica.herokuapp.com/" />
          <meta property="og:type" content="article" />
          <meta property="og:title" content="Diagnostica" />
          <meta property="og:description" content="Medical Diagnosis Tool with DSS" />
          <meta property="og:image" content={socialImg} />
        </Head>
        <Container style={{ padding: '2rem' }}>
          <Menu attached="top">
            <Menu.Item>
              <img src={logoImg} alt="" />
            </Menu.Item>
            <Menu.Item header>
              Diagnostica
            </Menu.Item>
            <Menu.Menu position="right">
              <Menu.Item>
                <Icon as={HelpCircle} />
              </Menu.Item>
            </Menu.Menu>
          </Menu>
          <Segment>
            <Progress active percent={100} color="blue" attached="top" />
            <Header content="Welcome to Diagnostica!" />
            <p>Decision Support Frame-Based Expert System for identifying possible conditions and treatment related to your symptoms.</p>
            <Step.Group fluid>
              <Step as={NavLink} exact to="/" activeClassName="active">
                <Icon as={Info} />
                <Step.Content>
                  <Step.Title>Info</Step.Title>
                </Step.Content>
              </Step>

              <Step as={NavLink} to="/symptoms/" activeClassName="active">
                <Icon as={CheckSquare} />
                <Step.Content>
                  <Step.Title>Symptoms</Step.Title>
                </Step.Content>
              </Step>

              <Step as={NavLink} to="/history/" activeClassName="active">
                <Icon as={Clock} />
                <Step.Content>
                  <Step.Title>History</Step.Title>
                </Step.Content>
              </Step>

              <Step as={NavLink} to="/conditions/" activeClassName="active">
                <Icon as={Activity} />
                <Step.Content>
                  <Step.Title>Conditions</Step.Title>
                </Step.Content>
              </Step>

              <Step as={NavLink} to="/details/" activeClassName="active">
                <Icon as={List} />
                <Step.Content>
                  <Step.Title>Details</Step.Title>
                </Step.Content>
              </Step>

              <Step as={NavLink} to="/treatment/" activeClassName="active">
                <Icon as={Heart} />
                <Step.Content>
                  <Step.Title>Treatment</Step.Title>
                </Step.Content>
              </Step>
            </Step.Group>

            <Router>
              <Switch>
                <Route exact path="/" component={InfoPage} />
                <Route exact path="/symptoms/" component={SymptomsPage} />
                <Route exact path="/history/" component={HistoryPage} />
                <Route exact path="/conditions/" component={ConditionsPage} />
                <Route exact path="/details/*" component={DetailsPage} />
                <Route exact path="/treatment/" component={TreatmentPage} />
              </Switch>
            </Router>

          </Segment>
          <p><small>A thesis by Cherry Joy A. Palomar | College of Computer Studies Graduate Programs | Central Philippine University | Copyright 2018</small></p>
        </Container>

      </Container>
    )
  }
}


export default withSiteData(() => (
  <Home />
))

