import React from 'react'
import { Router } from 'react-static'
import { hot } from 'react-hot-loader'
//
import Routes from 'react-static-routes'

import 'animate.css/animate.min.css'
import 'semantic-ui-css/semantic.min.css'
import './app.css'
// import Navigation from './components/Navigation'
// import withAuthentication from './session/withAuthentication'

const App = () => (
  <Router>
    <div className="content">
      <Routes />
    </div>
  </Router>
)

export default hot(module)(App)
