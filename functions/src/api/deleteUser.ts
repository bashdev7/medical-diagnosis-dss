import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

export default functions.https.onRequest((_request, _response) => {
    _response.set('Access-Control-Allow-Origin', '*')
    const { uid } = _request.query

    admin.auth().deleteUser(uid).then(result => {
        _response.status(200).send({ result })
    }).catch((err: any) => {
        _response.status(500).send({ err })
    })
})