import async from 'async';
import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import { spread, union } from 'lodash';
import { BayesClassifier } from 'natural';
const classifier = new BayesClassifier()


const db = admin.firestore()


function trainData() {
    db.collection("diseases").get().then(querySnapShot => {
        async.forEachOf(querySnapShot.docs, (doc, _key, callback) => {
            const { age_identifiers, bmi_identifiers, bp_identifiers, history_identifiers, symptoms_identifiers, key } = doc.data()
            const factors = [].concat.apply([], [age_identifiers, bmi_identifiers, bp_identifiers, history_identifiers, symptoms_identifiers])
            classifier.addDocument(factors, key)
            callback()
        }, error => {
            if (error) {
                console.log(error.message)
            } else {
                console.log("Starting training...")
                classifier.train()
            }
        })
    }).catch(error => {
        console.log(error)
    })
}

export default functions.https.onRequest((_req, _res) => {
    console.log({ headersSent: _res.headersSent })
    if (!_res.headersSent) {
        _res.set({ 'Access-Control-Allow-Origin': '*' })
    }
    classifier.events.removeListener('doneTraining', () => {
        console.log("Listener removed.")
    })
    const { age_identifiers, bmi_identifiers, bp_identifiers, history_identifiers, symptoms_identifiers } = _req.query
    const factor = spread(union)([[age_identifiers], [bmi_identifiers], [bp_identifiers], history_identifiers, symptoms_identifiers]) as any

    classifier.events.on('doneTraining', () => {
        if (!_res.headersSent) {
            _res.send({
                matches: classifier.getClassifications(factor).slice(0, 4),
            }).end()
        }
    })
    trainData()
})
