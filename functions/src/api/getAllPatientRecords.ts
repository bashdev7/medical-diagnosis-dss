import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import async from 'async';

export default functions.https.onRequest(async (_request, _response) => {
    _response.set('Access-Control-Allow-Origin', '*')
    const collection = admin.firestore().collection('users')
    const set: any = []
    await collection.get()
    .then((querySnapshot)=>{
        async.each(querySnapshot.docs, (doc, callback)=>{
            admin.firestore().doc(doc.ref.path).collection("data").get().then(item=>{
                item.forEach(i=>{
                    i.data().matches.map((j:any)=>{
                        set.push(j)
                    })
                    
                })
                callback()
            }).catch(err=>{
                console.log(err)
            })
        }, err=>{
            if( err ) {
                console.log(err)
            }else{
                _response.send(set)
            }
            
        })        
    })

})