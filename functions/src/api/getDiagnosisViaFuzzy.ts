import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import Fuse from 'fuse.js';

const db = admin.firestore()

export default functions.https.onRequest(async (_req, _res) => {
    console.log({ headersSent: _res.headersSent })
    if (!_res.headersSent) {
        _res.set({ 'Access-Control-Allow-Origin': '*' })
    }
    const options: Fuse.FuseOptions<any> = {
        keys: ['age_identifiers', 'bmi_identifiers', 'bp_identifiers', 'history_identifiers', 'symptoms_identifiers'],
        tokenize: true,
        shouldSort: true,
        threshold: 0.7,
        includeScore: true,
        maxPatternLength: 1000
    }
    await db.collection("diseases")
    .get().then((querySnapShot) => {
        const factors: any = []
        console.log({resultIsEmpty: querySnapShot.empty})
        querySnapShot.forEach(async (doc) => {
            const data = doc.data();
            // console.log(data);
            factors.push(data);
        })
        console.log("Starting fuzzy search...")
        console.log({ factors: JSON.stringify(factors) })
        const fuse = new Fuse(factors, options)
        const { age_identifiers, bmi_identifiers, bp_identifiers, history_identifiers, symptoms_identifiers } = _req.query
        const query = `${age_identifiers} ${bmi_identifiers} ${bp_identifiers} ${history_identifiers.join(" ")} ${symptoms_identifiers.join(" ")}`
        console.log({ query })
        const search = fuse.search(query)
        if (!_res.headersSent) {
            _res.send({
                matches: search
            }).end()
        }
    }).catch(error => {
        console.log(error)
    })
})