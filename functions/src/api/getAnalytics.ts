import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import cors from 'cors';
import express from 'express'
import dl from 'datalib'
const app = express();

app.use(cors({ origin: true }));

app.get('/', (req, res) => {
    const diseasesCollection = admin.firestore().collection('diseases')
    const set: any = []
    diseasesCollection.get()
        .then(documents => {
            documents.forEach(doc => {
                set.push(doc.data())
            })
        }).catch(error => {
            console.log(error)
        })

    let data: any = null
    if (req.query.model === 'diseases') {
        data = dl.json('https://us-central1-diagnostica-35f50.cloudfunctions.net/getAllDiseases')
    } else {
        data = dl.json('https://us-central1-diagnostica-35f50.cloudfunctions.net/getAllPatientRecords')
    }

    return res.send(dl.summary(data))
})


export default functions.https.onRequest(app)