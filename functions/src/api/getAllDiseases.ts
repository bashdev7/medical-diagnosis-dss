import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

export default functions.https.onRequest(async (_request, _response) => {
    _response.set('Access-Control-Allow-Origin', '*')
    const collection = admin.firestore().collection('diseases')
    const set: any = []
    await collection.get()
        .then(documents => {
            documents.forEach(doc => {
                set.push(doc.data())
            })
        })
    _response.send(set)
})