import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import { includes, intersection, sortBy } from 'lodash';

const db = admin.firestore()

export default functions.https.onRequest(async (_req, _res) => {
    console.log({ headersSent: _res.headersSent })
    if (!_res.headersSent) {
        _res.set({ 'Access-Control-Allow-Origin': '*' })
    }
    const { age_identifiers, bmi_identifiers, bp_identifiers, history_identifiers, symptoms_identifiers } = _req.query

    await db.collection("diseases")
        .get().then((querySnapShot) => {
            const factors: any = []
            console.log({ resultIsEmpty: querySnapShot.empty })

            querySnapShot.forEach(async (doc) => {
                const data = doc.data();
                let points = 0
                if (includes(age_identifiers, data.age_identifiers)) {
                    points = points + 20
                }
                if (includes(bmi_identifiers, data.bmi_identifiers)) {
                    points = points + 20
                }
                if (includes(bp_identifiers, data.bp_identifiers)) {
                    points = points + 20
                }
                const historyIntersection = intersection(history_identifiers, data.history_identifiers)
                console.log(historyIntersection.length, history_identifiers.length)
                if (historyIntersection.length >= 1) {
                    const historyScore = ((historyIntersection.length / history_identifiers.length) * 50 + 50) * 0.20
                    points += historyScore
                } else {
                    points += 10
                }

                const symptomsIntersection = intersection(symptoms_identifiers, data.symptoms_identifiers)
                if (symptomsIntersection.length >= 1) {
                    const symptomScore = ((symptomsIntersection.length / symptoms_identifiers.length) * 50 + 50) * 0.20
                    points += symptomScore
                } else {
                    points += 10
                }

                factors.push(Object.assign({}, { item: data }, { score: points }));
            })

            const search: any = sortBy(factors, ['score'])
            if (!_res.headersSent) {
                _res.send({
                    matches: search
                }).end()
            }
        }).catch(error => {
            console.log(error)
        })
})