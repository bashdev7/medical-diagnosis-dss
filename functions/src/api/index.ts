import createUser from './createUser'
import deleteUser from './deleteUser'
import getAllDiseases from './getAllDiseases'
import getAllPatientRecords from './getAllPatientRecords'
import getAnalytics from './getAnalytics'
import getDiagnosisViaFuzzy from './getDiagnosisViaFuzzy'
import getDiagnosisViaMatchScores from './getDiagnosisViaMatchScores'
import getDiagnosisViaNaiveBayes from './getDiagnosisViaNaiveBayes'
import getPDFReport from './getPDFReport'
import listUsers from './listUsers'

export { getAnalytics, getAllPatientRecords, getAllDiseases, deleteUser, getPDFReport, createUser, getDiagnosisViaMatchScores, getDiagnosisViaFuzzy, getDiagnosisViaNaiveBayes, listUsers }

