import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import async from 'async'

export default functions.https.onRequest(async (_request, _response) => {
    _response.set('Access-Control-Allow-Origin', '*')
    const { email, password, name, type } = _request.query

    async.series([
        function (callback) {
            admin.auth().createUser({
                email,
                password: password,
                displayName: name,
            }).then(result => {
                callback(null, result)
            }).catch((err: any) => {
                _response.status(500).send({ err })
            })
        }
    ], (err, result: any) => {
        console.log(result[0].uid)
        if (!err) {
            admin.firestore().collection("users").doc(result[0].uid).update({ type })
                .then(() => {
                    _response.status(200).send({ result: Object.assign({}, result[0], { type }) })
                })
                .catch((err2: any) => {
                    _response.status(500).send({ err2 })
                })
        }
    })
})