import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';

export default functions.https.onRequest((_request, _response) => {
    _response.set('Access-Control-Allow-Origin', '*')
    admin.auth().listUsers().then(result => {
        _response.status(200).send({ result })
    }).catch((err: any) => {
        _response.status(500).send({ err })
    })
})