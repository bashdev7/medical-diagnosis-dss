import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import PDFDocument from 'pdfkit';
import { logo } from '../logo';


const db = admin.firestore()

export default functions.https.onRequest((_req, _res) => {
    if (!_res.headersSent) {
        _res.set({ 'Access-Control-Allow-Origin': '*' })

    }
    const doc = new PDFDocument({ size: 'A4' });
    const { t, u } = _req.query // t=>timestamp, u=>user
    doc.pipe(_res);
    const img = new Buffer(logo, 'base64');
    doc.image(img, {
        fit: [150, 200],
    });
    console.log({ u })
    db.collection('users').doc(u).collection('data').get().then(res => {
        res.forEach(i => {
            const data = i.data()
            const { profile: { age_identifiers, bmi_identifiers, bp_identifiers, } } = data
            if ((data.timestamp as number).toString() === t) {
                doc.text(" ");
                doc.text(`Generated: ${(new Date(parseInt(t))).toISOString()}`);
                doc.text(" ");
                doc.text("================================================================");
                doc.text("Profile");
                doc.text("================================================================");
                doc.text(" ");
                doc.text(`Age: ${age_identifiers}`);
                doc.text(" ");
                doc.text(`BMI: ${bmi_identifiers}`);
                doc.text(" ");
                doc.text(`Blood Pressure: ${bp_identifiers}`);
                doc.text(" ");
                doc.text(" ");
                doc.text("================================================================");
                doc.text("Diagnosis");
                doc.text("================================================================");
                doc.text(" ");
                let count = 1
                data.matches.map((j:any) => {
                    doc.text(`${count}. ${j.name}`);
                    doc.text(" ");
                    doc.text(`About ${j.name}: ${j.description}`);
                    doc.text(" ");
                    doc.text(" ");
                    count+=1
                })
            }
        })
    }).then(() => {
        doc.end()
        _res.download('diagnosis.pdf');
    }).catch(err=>{
        console.log(err)
    })

})