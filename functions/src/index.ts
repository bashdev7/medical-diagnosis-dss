
import * as admin from 'firebase-admin';

import * as serviceAccount from './serviceAccountKey.json';
const cert = serviceAccount as any

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://diagnostica-35f50.firebaseio.com"
});

import { createUser, deleteUser, getAllDiseases, getAllPatientRecords, getAnalytics, getDiagnosisViaFuzzy, getDiagnosisViaMatchScores, getDiagnosisViaNaiveBayes, getPDFReport, listUsers } from './api';
export { deleteUser, getAllDiseases, getAllPatientRecords, getAnalytics, createUser, getDiagnosisViaFuzzy, getDiagnosisViaMatchScores, getDiagnosisViaNaiveBayes, getPDFReport, listUsers };

