import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/firestore";

const config = {
    apiKey: "AIzaSyD_fzYshBIJW_6q7c4yOqmQRYckgG3dkQM",
    authDomain: "diagnostica-35f50.firebaseapp.com",
    databaseURL: "https://diagnostica-35f50.firebaseio.com",
    projectId: "diagnostica-35f50",
    storageBucket: "diagnostica-35f50.appspot.com",
    messagingSenderId: "188848765734",
    appId: "1:188848765734:web:1b01b90cb89fac9bbf1fe7"
};

if (!firebase.apps.length) {
    firebase.initializeApp(config);
}

export const auth = firebase.auth();
export const authFacebook = new firebase.auth.FacebookAuthProvider();
export const authGoogle = new firebase.auth.GoogleAuthProvider();
export const db = firebase.database();
export const firestoreDB = firebase.firestore();