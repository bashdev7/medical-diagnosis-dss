import { db, firestoreDB } from "./firebase";
import { notification } from "antd";
import axios, { AxiosPromise } from 'axios';
import { functionsURL } from "../config";
// User API
export const doCreateUser = (id: string, username: string, email: string) =>
  db.ref(`users/${id}`).set({
    email,
    username
  });

export const onceGetUsers = () => db.ref("users").once("value");

export const timestamp = () => Date.now()

export const addNewData = (path: string, data: any) => {
  const transaction = firestoreDB.collection(path).doc()
  return { id: transaction.id, result: transaction.set(data) }
}

export const getUserType = (uid:string)=>{
  return firestoreDB.collection("users").doc(uid).get()
}

export const addNewUser = (email: string, password: string, type:string) => 
{
  console.log({type})
  return axios.get(`${functionsURL}createUser?email=${email}&password=${password}&type=${type}`)
}

export const addNewProfileData = (uid: string, data: any) => {
  return firestoreDB.collection('users').doc(uid).collection('data').add(data)
}

export const setUserType = (uid: string, type: string) => {
  return firestoreDB.collection('users').doc(uid).set({type})
}

export const getFieldName = (field: string, key: string) => {
  return firestoreDB.collection(field).doc(key).get()
}

export const updateNewData = (path: string, doc: string, data: any) => {
  console.log({ path, doc, data })
  try {
    return firestoreDB.collection(path).doc(doc).set(data)
  } catch (error) {
    notification.error({
      message: 'Sorry',
      description: error.message,
    })
  }
}


export const deleteDoc = (path: string, doc: string) => {
  return firestoreDB.collection(path).doc(doc).delete()
}

export const listAllUsers = () =>
  axios.get(`${functionsURL}listUsers`) as AxiosPromise

export const deleteUser = (uid: string) =>
  axios.get(`${functionsURL}deleteUser?uid=${uid}`)


export const getCollection = (path: string) => firestoreDB.collection(path)