import * as React from "react";
import * as routes from "../contants/routes";
import { firebase } from "../firebase";
import { AuthUserContext } from "./AuthUserContext";
import { navigate } from "@reach/router";

interface InterfaceProps {
    history?: any;
}

export const withAuthorization = (condition: any) => (Component: any) => {
    class WithAuthorization extends React.Component<InterfaceProps, {}> {
        public componentDidMount() {
            firebase.auth.onAuthStateChanged(authUser => {
                console.log({authUser})
                if (!condition(authUser)) {
                    navigate(`/${routes.SIGN_IN}`)
                }
            });
        }

        public render() {
            return (
                <AuthUserContext.Consumer>
                    {authUser => (authUser ? <Component /> : null)}
                </AuthUserContext.Consumer>
            );
        }
    }

    return WithAuthorization as any;
};