import { HistoryContext } from './HistoryContext'
import { DiseasesContext } from './DiseasesContext'
import { SymptomsContext } from './SymptomsContext'
import { PatientInfoContext } from './PatientInfoContext'
export {
    HistoryContext, DiseasesContext, SymptomsContext, PatientInfoContext
}