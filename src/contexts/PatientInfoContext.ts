import { createContext } from 'react'
export const PatientInfoContext = createContext({
    age: null,
    gender: null,
    height: null,
    weight: null,
    systolic: null,
    diastolic: null,
    updateInfo: (_name: string, _value: any) => { }
} as any)