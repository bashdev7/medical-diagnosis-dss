import { navigate } from '@reach/router';
import React, { Component } from 'react';
import * as auth from '../firebase/auth';
import { withAuthorization } from '../firebase/withAuthorization';
import { getUserType } from '../firebase/db';

class index extends Component<any, any> {
  constructor(props: any) {
    super(props)
  }

  componentDidMount() {
    const user = auth.getCurrentUser()
    if (user) {
      getUserType(user.uid).then(value => {
        if (value.data() && value.data().type == "patient") {
          navigate("/patient")
        } else {
          navigate("/doctor")
        }
      })
    }
  }

  render() {
    return <React.Fragment>{this.props.children}</React.Fragment>
  }
}

const authCondition = (authUser: any) => !!authUser;

export default withAuthorization(authCondition)(index);