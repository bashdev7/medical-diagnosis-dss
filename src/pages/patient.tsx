import { navigate, Router } from '@reach/router';
import { Icon, Layout, Menu } from 'antd';
import React, { Component } from 'react';
import PatientAnalytics from '../components/PatientAnalytics';
import PatientDiagnosis from '../components/PatientDiagnosis';
import PatientProfile from '../components/PatientProfile';
import { HistoryContext, SymptomsContext } from '../contexts';
import { db } from '../firebase';
import * as auth from '../firebase/auth';

const { Header, Sider, Content } = Layout;

const PatientProfilePage = (props: any) => <PatientProfile {...props} />
const PatientDiagnosisPage = (props: any) => <PatientDiagnosis {...props} />
const PatientAnalyticsPage = (props: any) => <PatientAnalytics {...props} />

class index extends Component<any, any> {
    _isMounted: boolean;
    _historyListener: any;
    _symptomsListener: any;

    constructor(props: any) {
        super(props)
        this.state = {
            collapsed: false,
            history: null,
            symptoms: null,
        }
        this._isMounted = false;
        this._historyListener = null
        this._symptomsListener = null
    }

    componentDidMount() {
        this._isMounted = true;
        if (this._isMounted) {
            this._historyListener = db.getCollection("history").onSnapshot((doc: any) => {
                const historyBucket: any = []
                doc.forEach((i: any) => {
                    historyBucket.push(i.data())
                })
                this.setState({ history: historyBucket })
            })
            this._symptomsListener = db.getCollection("symptoms").onSnapshot((doc: any) => {
                const symptomsBucket: any = []
                doc.forEach((i:any) => {
                    symptomsBucket.push(i.data())
                })
                this.setState({ symptoms: symptomsBucket })
            })
            navigate('/patient/profile')
        }

    }

    componentWillUnmount() {
        this._isMounted = false;
        // Unsubscribe for Firestore
        this._historyListener()
        this._symptomsListener()
    }

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    gotoPage = (path: string) => {
        navigate(path)
    }
    render() {
        const { collapsed, history, symptoms } = this.state
        return (
            <HistoryContext.Provider value={history}>
                <SymptomsContext.Provider value={symptoms}>
                    <Layout>
                        <Sider trigger={null} collapsible collapsed={collapsed}>
                            <div className="center">
                                {collapsed ?
                                    <img src={require('../assets/logo-light--icon.svg')} className="logo" alt="" /> :
                                    <img src={require('../assets/logo-light.svg')} className="logo" alt="" />
                                }
                            </div>
                            <Menu theme="dark" mode="inline" selectedKeys={[((this.props.location.pathname) as string).split("/")[2]]}>
                                <Menu.Item key="profile" onClick={() => this.gotoPage("/patient/profile")}>
                                    <Icon type="user" />
                                    <span>My Profile</span>
                                </Menu.Item>
                                <Menu.Item key="diagnosis" onClick={() => this.gotoPage("/patient/diagnosis")}>
                                    <Icon type="heart" />
                                    <span>Diagnosis</span>
                                </Menu.Item>
                                <Menu.Item key="analytics" onClick={() => this.gotoPage("/patient/analytics")}>
                                    <Icon type="area-chart" />
                                    <span>Analytics</span>
                                </Menu.Item>
                                <Menu.Item key="logout" onClick={() => auth.doSignOut()}>
                                    <Icon type="logout" />
                                    <span>Sign Out</span>
                                </Menu.Item>
                            </Menu>
                        </Sider>
                        <Layout>
                            <Header style={{ width: '100%', backgroundColor: '#fff', padding: 0 }}>
                                <span>
                                    <Icon
                                        className="trigger"
                                        type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                                        onClick={this.toggle}
                                    />
                                    Welcome Patient!
                                </span>
                            </Header>
                            <Content
                                style={{
                                    margin: '24px 16px',
                                    padding: 24,
                                    background: '#fff',
                                    minHeight: '90vh',
                                }}
                            >
                                <Router>
                                    <PatientProfilePage path="profile" />
                                    <PatientDiagnosisPage path="diagnosis" />
                                    <PatientAnalyticsPage path="analytics" />
                                </Router>
                            </Content>
                        </Layout>
                    </Layout>
                </SymptomsContext.Provider>
            </HistoryContext.Provider>
        );
    }
}

export default index;