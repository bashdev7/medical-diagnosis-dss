import { navigate, Router } from '@reach/router';
import { Icon, Layout, Menu } from 'antd';
import React, { Component } from 'react';
import DiseasesManager from '../components/DiseasesManager';
import HistoryManager from '../components/HistoryManager';
import PatientsManager from '../components/PatientsManager';
import SymptomsManager from '../components/SymptomsManager';
import { age, bp } from '../config';
import { HistoryContext, SymptomsContext } from '../contexts';
import { db } from '../firebase';
import * as auth from '../firebase/auth';

const { Header, Sider, Content } = Layout;


const SymptomsManagerPage = (props: any) => <SymptomsManager {...props} />
const HistoryManagerPage = (props: any) => <HistoryManager {...props} />
const DiseasesManagerPage = (props: any) => <DiseasesManager {...props} />
const PatientsManagerPage = (props: any) => <PatientsManager {...props} />
class index extends Component<any, any> {
  constructor(props: any) {
    super(props)
    this.state = {
      collapsed: false,
      symptoms: null,
      age,
      bp,
      history: null,
    }
  }

  componentDidMount() {
    navigate("/doctor/history")
    db.getCollection("history").onSnapshot(doc => {
      const historyBucket: any = []
      doc.forEach(i => {
        historyBucket.push(i.data())
      })
      this.setState({ history: historyBucket })
    })
    db.getCollection("symptoms").onSnapshot(doc => {
      const symptomsBucket: any = []
      doc.forEach(i => {
        symptomsBucket.push(i.data())
      })
      this.setState({ symptoms: symptomsBucket })
    })
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  gotoPage = (path: string) => {
    navigate(path)
  }
  render() {
    const { collapsed, history, symptoms } = this.state
    return (
      <Layout>
        <HistoryContext.Provider value={history}>
          <SymptomsContext.Provider value={symptoms}>
            <Sider trigger={null} collapsible collapsed={collapsed}>
              <div className="center">
                {collapsed ?
                  <img src={require('../assets/logo-light--icon.svg')} className="logo" alt="" /> :
                  <img src={require('../assets/logo-light.svg')} className="logo" alt="" />
                }
              </div>
              <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                <Menu.Item key="1" onClick={() => this.gotoPage("/doctor/symptoms")}>
                  <Icon type="unordered-list" />
                  <span>Symptoms</span>
                </Menu.Item>
                <Menu.Item key="2" onClick={() => this.gotoPage("/doctor/history")}>
                  <Icon type="hourglass" />
                  <span>History</span>
                </Menu.Item>
                <Menu.Item key="3" onClick={() => this.gotoPage("/doctor/diseases")}>
                  <Icon type="heart" />
                  <span>Diseases</span>
                </Menu.Item>
                <Menu.Item key="4" onClick={() => this.gotoPage("/doctor/patients")}>
                  <Icon type="usergroup-add" />
                  <span>Users</span>
                </Menu.Item>
                <Menu.Item key="5" onClick={() => {
                  auth.doSignOut().then(()=>{
                    this.gotoPage("/")
                  })                  
                }}>
                  <Icon type="logout" />
                  <span>Sign Out</span>
                </Menu.Item>
              </Menu>
            </Sider>
            <Layout>
              <Header style={{ width: '100%', backgroundColor: '#fff', padding: 0 }}>
                <span>
                  <Icon
                    className="trigger"
                    type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                    onClick={this.toggle}
                  />
                  Welcome Doctor!
            </span>
              </Header>
              <Content
                style={{
                  margin: '24px 16px',
                  padding: 24,
                  background: '#fff',
                  minHeight: '90vh',
                }}
              >
                <Router>
                  <SymptomsManagerPage path="symptoms" />
                  <HistoryManagerPage path="history" />
                  <DiseasesManagerPage path="diseases" />
                  <PatientsManagerPage path="patients" />
                </Router>
              </Content>
            </Layout>
          </SymptomsContext.Provider>
        </HistoryContext.Provider>
      </Layout>
    );
  }
}

export default index;