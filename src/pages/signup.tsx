import { Button, Form, Icon, Input, Alert } from "antd";
import React, { Component } from 'react';
import * as auth from '../firebase/auth'
import { navigate } from "@reach/router";
import * as routes from '../contants/routes'
const FormItem = Form.Item;
class index extends Component<any, any> {
    constructor(props: any) {
        super(props)
        this.state = {
            error: null,
            loggingIn: false
        }
    }
    handleSubmit = (e: any) => {
        e.preventDefault();
        this.setState({ loggingIn: true })
        this.props.form.validateFields((err: any, values: any) => {
            if (!err) {
                const { userEmail, password, password_confirm } = values
                if (password === password_confirm) {
                    auth.doCreateUserWithEmailAndPassword(userEmail, password).then((user: any) => {
                        if (user) {
                            this.setState({ loggingIn: false })
                            navigate(routes.LANDING)
                        }
                    }).catch((error: any) => {
                        this.setState({ loggingIn: false })
                        this.setState({ error })
                    })
                } else {
                    this.setState({ loggingIn: false })
                    this.setState({ error: { message: "Sorry, password did not matched. Please try again." } })
                }

            } else {
                this.setState({ loggingIn: false })
            }
        });
    };
    render() {
        const { getFieldDecorator } = this.props.form;
        const { error, loggingIn } = this.state
        return (
            <div className="background-gfx" style={{ backgroundImage: `url(${require('../assets/undraw_medicine_b1ol.svg')})` }}>
                <div className="container">
                    <div className="form-container">
                        <img className="app-logo" src={require('../assets/logo.svg')} />
                        <h3>Sign Up to Diagnostica</h3>
                        <Form onSubmit={this.handleSubmit} className="login-form">
                            <FormItem>
                                {getFieldDecorator("userEmail", {
                                    rules: [
                                        { required: true, message: "Please input your email!" },
                                    ]
                                })(
                                    <Input
                                        prefix={<Icon type="user" style={{ fontSize: 13 }} />}
                                        placeholder="Email"
                                    />
                                )}
                            </FormItem>

                            <FormItem>
                                {getFieldDecorator("password", {
                                    rules: [{ required: true, message: "Please input your Password!" }]
                                })(
                                    <Input
                                        prefix={<Icon type="lock" style={{ fontSize: 13 }} />}
                                        type="password"
                                        placeholder="Password"
                                    />
                                )}
                            </FormItem>
                            <FormItem>
                                {getFieldDecorator("password_confirm", {
                                    rules: [{ required: true, message: "Please input your Password!" }]
                                })(
                                    <Input
                                        prefix={<Icon type="lock" style={{ fontSize: 13 }} />}
                                        type="password"
                                        placeholder="Confirm Password"
                                    />
                                )}
                            </FormItem>
                            {error && <FormItem style={{textAlign: 'left'}}><Alert closable showIcon type="error" message={error.message} /></FormItem>}
                            <FormItem>
                                <Button.Group className="login-form-button">
                                    <Button
                                        loading={loggingIn}
                                        onClick={() => navigate("/signin")}
                                        style={{ width: "50%" }}
                                    >
                                        Cancel
                                </Button>
                                    <Button
                                        loading={loggingIn}
                                        type="primary"
                                        htmlType="submit"
                                        style={{ width: "50%" }}
                                    >
                                        Sign Up
                                </Button>
                                </Button.Group>
                            </FormItem>
                        </Form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Form.create()(index);

