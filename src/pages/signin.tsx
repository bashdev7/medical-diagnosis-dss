import { Button, Form, Icon, Input, Alert, Divider } from "antd";
import React, { Component } from 'react';
import * as auth from '../firebase/auth'
import { navigate, Link } from "@reach/router";
import * as routes from '../contants/routes'
const FormItem = Form.Item;
class index extends Component<any, any> {
    constructor(props: any) {
        super(props)
        this.state = {
            error: null,
            loggingIn: false
        }
    }
    handleSubmit = (e: any) => {
        e.preventDefault();
        this.setState({ loggingIn: true })
        this.props.form.validateFields((err: any, values: any) => {
            if (!err) {
                const { userEmail, password } = values
                auth.doSignInWithEmailAndPassword(userEmail, password).then((user: any) => {
                    if (user) {
                        this.setState({ loggingIn: false })
                        navigate(routes.LANDING)
                    }
                }).catch((error: any) => {
                    this.setState({ loggingIn: false })
                    this.setState({ error })
                })
            } else {
                this.setState({ loggingIn: false })
            }
        });
    };

    signInWithProvider = (provider: string) => {
        this.setState({ loggingIn: true })
        switch (provider) {
            case "facebook":
                auth.signInWithFacebook().then((result: any) => {
                    var user = result.user;
                    console.log({ user })
                    this.setState({ loggingIn: false })
                }).catch((error: any) => {
                    this.setState({ loggingIn: false })
                    this.setState({ error })
                })
                break;

            default:
                auth.signInWithGoogle().then((result: any) => {
                    var user = result.user;
                    console.log({ user })
                    this.setState({ loggingIn: false })
                }).catch((error: any) => {
                    this.setState({ loggingIn: false })
                    this.setState({ error })
                })
                break;
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const { error, loggingIn } = this.state
        return (
            <div className="background-gfx" style={{ backgroundImage: `url(${require('../assets/undraw_medicine_b1ol.svg')})` }}>
                <div className="container">
                    <div className="form-container">
                        <img className="app-logo" src={require('../assets/logo.svg')} />
                        <h3>Diagnostica</h3>
                        <Form onSubmit={this.handleSubmit} className="login-form">
                            <FormItem>
                                {getFieldDecorator("userEmail", {
                                    rules: [
                                        { required: true, message: "Please input your email!" },
                                    ]
                                })(
                                    <Input
                                        prefix={<Icon type="user" style={{ fontSize: 13 }} />}
                                        placeholder="Email"
                                    />
                                )}
                            </FormItem>

                            <FormItem>
                                {getFieldDecorator("password", {
                                    rules: [{ required: true, message: "Please input your Password!" }]
                                })(
                                    <Input
                                        prefix={<Icon type="lock" style={{ fontSize: 13 }} />}
                                        type="password"
                                        placeholder="Password"
                                    />
                                )}
                            </FormItem>
                            {error && <FormItem style={{ textAlign: 'left' }}><Alert closable showIcon type="error" message={error.message} /></FormItem>}
                            <FormItem>
                                <Button
                                    loading={loggingIn}
                                    type="primary"
                                    htmlType="submit"
                                    className="login-form-button"
                                >
                                    Log in
                                </Button>
                            </FormItem>
                            <Divider />
                            <FormItem>
                                <p>Don't have an account yet? <Link to="/signup">Sign Up.</Link></p>
                                {/* <Button
                                    loading={loggingIn}
                                    icon="facebook"
                                    className="login-form-button"
                                    onClick={() => this.signInWithProvider('facebook')}
                                >
                                    Log in with Facebook
                                </Button>
                                <Button
                                    loading={loggingIn}
                                    icon="google"
                                    className="login-form-button"
                                    onClick={() => this.signInWithProvider('google')}
                                >
                                    Log in with Google
                                </Button> */}
                            </FormItem>
                        </Form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Form.create()(index);

