import { reactLocalStorage } from 'reactjs-localstorage';
export const getInfoData = async (path: string) => {
    return new Promise((accept, reject) => {
        try {
            const data = reactLocalStorage.getObject(path);
            if (data) {
                accept(data);
            }
            else {
                reject();
            }
        }
        catch (error) {
            console.log(error);
            reject();
        }
    });
};
