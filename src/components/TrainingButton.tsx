import React, { Component } from 'react';
import { Button } from 'antd';
import axios from 'axios';
import { functionsURL } from '../config';

class TrainingButton extends Component<any, any> {
    constructor(props: any) {
        super(props)
        this.state = {
            loading: false,
            trainingState: 'Start Training',
            type: 'primary'
        }
    }

    train = () => {
        const waitTime = setTimeout(() => {
            this.setState({ loading: false, trainingState: 'Timeout Reached. Try again?', type: 'danger' })
        }, 30000);
        this.setState({ loading: true, trainingState: 'Training...', type: 'default' })
        axios.get(`${functionsURL}trainData`).then(res => {
            if (res.data) {
                clearTimeout(waitTime)
                this.setState({ loading: false, trainingState: 'Done Training!', type: 'primary' })
            }
            setTimeout(() => {
                // Reset text
                this.setState({ trainingState: 'Start Training', type: 'primary' })
            }, 2000)
        })
    }

    render() {
        const { loading, trainingState, type } = this.state
        return (
            <Button loading={loading} onClick={this.train} size="large" type={type}>{trainingState}</Button>
        );
    }
}

export default TrainingButton;