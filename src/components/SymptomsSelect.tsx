import { Select, Spin, Tooltip } from 'antd';
import React, { Component } from 'react';
import { SymptomsContext } from '../contexts/';
const { Option } = Select;
class SymptomsSelect extends Component<any,any> {
    render() {
        let props = this.props;
        let symptoms = this.context;
        return (<React.Fragment>
            {symptoms ?
                <Select {...props} showSearch style={{ width: '100%' }} placeholder="Select a Category" optionFilterProp="children" mode="multiple">
                    {Object.keys(symptoms).map((i: any) => {
                        // console.log({ symptoms });
                        const { name, key, description } = symptoms[i];
                        return name && key ? <Option key={i} value={key}><Tooltip title={description}>{name}</Tooltip></Option> : null;
                    })}
                </Select> : <Spin spinning />}
        </React.Fragment>);
    }
}
SymptomsSelect.contextType = SymptomsContext;

export default SymptomsSelect
