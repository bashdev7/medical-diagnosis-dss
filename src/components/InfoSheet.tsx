import { Button, Col, Form, Icon, InputNumber, Row, Select, Alert, Divider } from 'antd';
import React, { Component } from 'react';
import { reactLocalStorage } from 'reactjs-localstorage'
import { PatientInfoContext } from '../contexts';
import { getInfoData } from './getInfoData';
const { Option } = Select;

interface InfoSheetTypes {
    age: any,
    gender: any,
    height: any,
    weight: any,
    systolic: any,
    diastolic: any,
    updateInfo?: any
}

class index extends Component<any, InfoSheetTypes> {
    handleInputChange: (name: string, value: any) => void;
    constructor(props: any) {
        super(props)

        this.handleInputChange = (name: string, value: any) => {
            this.setState({ [name]: value } as any, () => reactLocalStorage.setObject('diagnostica_info', this.state))
        }

        this.state = {
            age: undefined,
            gender: undefined,
            height: undefined,
            weight: undefined,
            systolic: undefined,
            diastolic: undefined,
            updateInfo: this.handleInputChange
        }
    }

    componentDidMount() {
        getInfoData('diagnostica_info').then((data: any) => {
            const { age, gender, height, weight, systolic, diastolic } = data
            this.setState({
                age,
                gender,
                height,
                weight,
                systolic,
                diastolic,
            } as any)
        })
    }

    clearStorage = () => {
        reactLocalStorage.clear()
        getInfoData('diagnostica_info').then(() => {
            this.setState({
                age: undefined,
                gender: undefined,
                height: undefined,
                weight: undefined,
                systolic: undefined,
                diastolic: undefined,
                updateInfo: this.handleInputChange
            })
        })
    }



    render() {
        return (
            <PatientInfoContext.Provider value={this.state}>
                <PatientInfoContext.Consumer>
                    {
                        value => {
                            const { age, gender, height, weight, systolic, diastolic, updateInfo } = value
                            return (
                                <div className="sub-form">
                                    <Form>
                                        <Row gutter={[50, 16]}>
                                            <Col span={12}>
                                                <img style={{ width: 'auto', height: '486px', margin: '0 auto' }} src={require('../assets/undraw_medicine_b1ol2.svg')} />
                                                <Alert type="info" showIcon message="This tool does not provide medical advice It is intended for informational purposes only. It is not a substitute for professional medical advice, diagnosis or treatment. Never ignore professional medical advice in seeking treatment." />
                                            </Col>
                                            <Col span={12} >
                                                <Row gutter={20}>
                                                    <Col span={12}>
                                                        <h3>Basic Info</h3>
                                                        <Divider />
                                                        <span>Age</span>
                                                        <Form.Item>
                                                            <Icon type="user" style={{ color: 'rgba(0,0,0,.25)', marginRight: '1rem' }} />
                                                            <InputNumber                                                            
                                                                style={{ width: '80%' }}
                                                                onChange={(value: any) => updateInfo("age", value)}
                                                                placeholder="Age"
                                                                name="age"
                                                                value={age}
                                                            />
                                                        </Form.Item>
                                                        <span>Gender</span>
                                                        <Form.Item>
                                                            <Icon type="user" style={{ color: 'rgba(0,0,0,.25)', marginRight: '1rem' }} />
                                                            <Select
                                                                value={gender}
                                                                placeholder="Gender"
                                                                style={{ width: '80%' }}
                                                                onSelect={(value: string) => updateInfo("gender", value)}>
                                                                <Option value="male">Male</Option>
                                                                <Option value="female">Female</Option>
                                                            </Select>
                                                        </Form.Item>
                                                        <span>Height (m)</span>
                                                        <Form.Item>
                                                            <Icon type="user" style={{ color: 'rgba(0,0,0,.25)', marginRight: '1rem' }} />
                                                            <InputNumber
                                                                value={height}
                                                                style={{ width: '80%' }}
                                                                onChange={(value: any) => updateInfo("height", value)}
                                                                placeholder="Height (m)"
                                                            />
                                                        </Form.Item>
                                                        <span>Weight (kg)</span>
                                                        <Form.Item>
                                                            <Icon type="user" style={{ color: 'rgba(0,0,0,.25)', marginRight: '1rem' }} />
                                                            <InputNumber
                                                                value={weight}
                                                                style={{ width: '80%' }}
                                                                placeholder="Weight (kg)"
                                                                onChange={(value: any) => updateInfo("weight", value)}
                                                            />
                                                        </Form.Item>
                                                    </Col>
                                                    <Col span={12}>
                                                        <h3>Blood Pressure</h3>
                                                        <Divider />
                                                        <span>Systolic</span>
                                                        <Form.Item>
                                                            <Icon type="user" style={{ color: 'rgba(0,0,0,.25)', marginRight: '1rem' }} />
                                                            <InputNumber
                                                                value={systolic}
                                                                style={{ width: '80%' }}
                                                                onChange={(value: any) => updateInfo("systolic", value)}
                                                                placeholder="Systolic"
                                                            />
                                                        </Form.Item>
                                                        <span>Diastolic</span>
                                                        <Form.Item>
                                                            <Icon type="user" style={{ color: 'rgba(0,0,0,.25)', marginRight: '1rem' }} />
                                                            <InputNumber
                                                                value={diastolic}
                                                                style={{ width: '80%' }}
                                                                onChange={(value: any) => updateInfo("diastolic", value)}
                                                                placeholder="Diastolic"
                                                            />
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col> <Button style={{ width: '100%' }} icon="reload" onClick={() => { this.clearStorage() }}>Reset My Info</Button></Col>
                                                </Row>
                                            </Col>

                                        </Row>
                                    </Form>
                                    <br />
                                </div>
                            )
                        }
                    }
                </PatientInfoContext.Consumer>
            </PatientInfoContext.Provider>
        )
    }
}

index.contextType = PatientInfoContext;

export default Form.create()(index);
