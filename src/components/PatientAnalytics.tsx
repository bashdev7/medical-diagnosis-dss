import { Empty, PageHeader, Spin, Divider } from 'antd';
import { waterfall } from 'async';
import { omitBy, pickBy, upperCase } from 'lodash';
import randomColor from 'randomcolor';
import React, { Component } from 'react';
import { Bar, Doughnut } from 'react-chartjs-2';
import { db } from '../firebase';
import { getAnalytics } from '../utils';


class index extends Component<any, any> {
    constructor(props: any) {
        super(props)
        this.state = {
            loading: true,
            data: null,
            componentSet: null
        }
    }

    componentDidMount() {
        this.getAnalyticsData()
    }

    getAnalyticsData() {
        console.log("Getting data...")
        const component: any = []
        waterfall([
            (callback: Function) => {
                getAnalytics("records").then((value: any) => {
                    // Filtered data     
                    console.log("===========================");
                    console.log(value);
                    console.log("===========================");
                    // let records = await filter(value, o => o.field !== 'description' && o.field !== 'treatment' && o.field !== 'key')
                    let description = omitBy(value, o => o.field === "description")
                    let treatment = omitBy(description, o => o.field === "treatment")
                    let key = omitBy(treatment, o => o.field === "key")
                    callback(null, key)
                })
            },
            (data: any, callback: Function) => {
                console.log("===========================");
                console.log(data);
                console.log("===========================");
                Promise.all(Object.keys(data).map((i: any) => {
                    return data[i] !== true && this.createDataSet(data, data[i].field)
                })).then(dataSet => {
                    callback(null, dataSet)
                })
            }
        ], (_err, result: any) => {
            console.log({ result })
            result.map((i: any) => {
                if (i.fieldName === 'name') {
                    component.push(
                        <React.Fragment>
                            <div style={{ marginBottom: '2em' }} key={i.fieldName}>
                                <p style={{ fontWeight: 'bold' }}>{upperCase('Disease Frequency')}</p>
                                <Bar data={i.data} />
                            </div>
                            <Divider />
                        </React.Fragment>
                    )
                } else {
                    component.push(
                        <React.Fragment>
                            <div style={{ marginBottom: '2em' }} key={i.fieldName}>
                                <p style={{ fontWeight: 'bold' }}>{upperCase(i.fieldName)}</p>
                                <Doughnut data={i.data} />
                            </div>
                            <Divider />
                        </React.Fragment>
                    )
                }
            })
            this.setState({
                data: result,
                componentSet: component,
                loading: false
            })
        })
    }

    populateComponents = async (data: any) => {
        return Promise.all(data.map(async (i: any) => await this.createDataSet(data, i.fieldName)))
    }

    lookUpNames = async (field: string, key: string) => {
        const keys = key.split(",")
        return Promise.all(keys.map(i => {
            if (field === 'history_identifiers') return db.getFieldName('history', i)
            if (field === 'symptoms_identifiers') return db.getFieldName('symptoms', i)
            return new Promise((resolve => resolve({ data: () => i })))
        }))
    }

    createDataSet = (entries: any, field: string) => new Promise(async (_resolve, _reject) => {
        const fieldName = field.split("_")[0]
        const set: any = Object.values(pickBy(entries, o => o.field === field))[0]
        const labels = Promise.all(Object.keys(set.unique).map(async i => {
            return await this.lookUpNames(field, i);
        }))

        labels.then(labelSet => {
            let labelsFull = labelSet.map(i => i.map((j: any) => j.data()))
            const data = Object.keys(set.unique).map((j: any) => set.unique[j])
            const backgroundColor = Object.keys(labelsFull).map(() => randomColor())
            const returnData = {
                labels: labelsFull.map((i) => {
                    return i.map(j => {
                        console.log({ j })
                        return j ? j["name"] || j : 'None'
                    }) || i
                }),
                datasets: [{
                    label: field === 'name' ? 'Disease Frequency' : field,
                    data,
                    backgroundColor,
                    hoverBackgroundColor: backgroundColor
                }]
            }
            _resolve(Object.assign({}, { data: returnData }, { fieldName }))
        })


    })

    render() {
        const { loading, componentSet } = this.state
        return (
            <div>
                <PageHeader title="Patients Analytics" subTitle="Data Analytics from Diagnostica Server">
                    <Spin spinning={loading}>
                        <div>
                            {componentSet ? componentSet : <Empty />}
                        </div>
                    </Spin>
                </PageHeader>
            </div>
        );
    }
}

export default index;