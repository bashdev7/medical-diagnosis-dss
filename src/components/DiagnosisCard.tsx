import { Card, Progress, Skeleton, Icon, Modal, Divider, Tag, Row, Col } from 'antd';
import React, { Component } from 'react';
import { getCollection } from '../firebase/db';
import { includes, intersection, filter, startCase } from 'lodash'
import { Radar } from 'react-chartjs-2'

class DiagnosisCard extends Component<any, any> {
    constructor(props: any) {
        super(props)
        this.state = {
            name: 'Loading...',
            description: '',
            loading: true,
            percent: 0,
            visible: false,
            history: this.props.meta[0],
            symptoms: this.props.meta[1],
            ageScore: 0,
            bmiScore: 0,
            bpScore: 0,
            historyScore: 0,
            symptomScore: 0
        }
    }
    componentDidMount() {
        const { docID, userData, score } = this.props
        getCollection('diseases').doc(docID).get().then(res => {
            const { name, description, age_identifiers, bmi_identifiers, bp_identifiers, history_identifiers, symptoms_identifiers, treatment } = res.data()
            let points = 0
            if (includes(age_identifiers, userData.age_identifiers)) {
                points += 20
                this.setState({ ageScore: 20 })
            }
            if (includes(bmi_identifiers, userData.bmi_identifiers)) {
                points += 20
                this.setState({ bmiScore: 20 })
            }
            if (includes(bp_identifiers, userData.bp_identifiers)) {
                points += 20
                this.setState({ bpScore: 20 })
            }
            const historyIntersection = intersection(history_identifiers, userData.history_identifiers)
            console.log(historyIntersection.length, history_identifiers.length)
            if (historyIntersection.length >= 1) {
                const historyScore = ((historyIntersection.length / history_identifiers.length) * 50 + 50) * 0.20
                points += historyScore
                this.setState({ historyScore })
            }else{
                points += 10
                this.setState({ historyScore:10 })
            }

            const symptomsIntersection = intersection(symptoms_identifiers, userData.symptoms_identifiers)
            if (symptomsIntersection.length >= 1) {
                const symptomScore = ((symptomsIntersection.length / symptoms_identifiers.length) * 50 + 50) * 0.20
                points += symptomScore
                this.setState({ symptomScore })
            }else{
                points += 10
                this.setState({ symptomScore:10 })
            }

            console.log( points.toFixed(2))
            this.setState({
                name,
                description,
                loading: false,
                percent: score.toFixed(2),
                treatment,
                age_identifiers,
                bmi_identifiers,
                bp_identifiers,
                history_identifiers,
                symptoms_identifiers,

            })
        })

    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    };

    handleOk = (e: any) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    handleCancel = (e: any) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };


    render() {
        const { name, description, loading, percent, visible, treatment,
            age_identifiers,
            bmi_identifiers,
            bp_identifiers,
            history_identifiers,
            symptoms_identifiers,
            history,
            symptoms,
            ageScore,
            bmiScore,
            bpScore,
            historyScore,
            symptomScore,
        } = this.state
        return (
            <React.Fragment>
                <Modal
                    title={name}
                    visible={visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    cancelText="Close"
                >
                    <h3>About</h3>
                    <p>{description}</p>
                    <Divider />
                    <h3>Treatment</h3>
                    <p>{treatment}</p>
                    <Divider />
                    <h3>Factors</h3>
                    <p>Affects Ages: {age_identifiers && age_identifiers.map((i: string) => <Tag color="blue">{startCase(i)}</Tag>)}</p>
                    <p>Affects People with BMI: {bmi_identifiers && bmi_identifiers.map((i: string) => <Tag color="blue">{startCase(i)}</Tag>)}</p>
                    <p>Blood Pressure Rages: {bp_identifiers && bp_identifiers.map((i: string) => <Tag color="blue">{startCase(i)}</Tag>)}</p>                   
                    <p>Affect People with History: {
                        <React.Fragment>
                            {
                                history ? history_identifiers && history_identifiers.map((i: string) => {
                                    const item = filter(history, o => o.key === i)[0]
                                    return item && <Tag color="blue">{item.name}</Tag>
                                }) : ""
                            }
                        </React.Fragment>
                    }</p>
                    <p>Identified Symptoms: {
                        <React.Fragment>
                            {
                                symptoms ? symptoms_identifiers && symptoms_identifiers.map((i: string) => {
                                    const item = filter(symptoms, o => o.key === i)[0]
                                    return item && <Tag color="blue">{item.name}</Tag>
                                }) : ""
                            }
                        </React.Fragment>
                    }</p>
                </Modal>
                <Card
                    size={percent >= 60 ? "default" : "small"}
                    title={`${name}`}
                    bordered={true}
                    style={{ width: 550, marginBottom: '1rem', border: percent >= 60 ? '2px solid #87d068' : '1px solid rgba(0,0,0,0.1)', opacity: percent >= 60 ? '1' : '0.50', borderRadius: '3px' }}
                    actions={[
                        <Icon onClick={() => this.showModal()} type="search" key="search" />,
                    ]}
                >
                    <Row>
                        <Col span={10}>
                            <Progress strokeColor={{
                                '0%': '#108ee9',
                                '100%': '#87d068',
                            }}
                                percent={percent}
                                showInfo={false}
                            />
                            {loading ? <Skeleton active paragraph /> : <Card.Meta title={`Match: ${percent}%`} description={description} />}
                        </Col>
                        <Col span={14}>
                            <Radar height={200} legend={false} data={{
                                labels: ["Age", "BMI", "BP", "History", "Symptoms"],
                                datasets: [
                                    {
                                        label: "Distribution",
                                        backgroundColor: "rgba(185,217,85,0.5)",
                                        borderColor: "#108ee9",
                                        pointBackgroundColor: "rgba(185,217,85,1)",
                                        pointBorderColor: "#108ee9",
                                        pointHoverBackgroundColor: "#108ee9",
                                        pointHoverBorderColor: "rgba(179,181,198,1)",
                                        data: [
                                            ageScore,
                                            bmiScore,
                                            bpScore,
                                            historyScore,
                                            symptomScore,
                                        ]
                                    },
                                ]
                            }} />
                        </Col>
                    </Row>
                </Card>
            </React.Fragment>

        );
    }
}

export default DiagnosisCard;