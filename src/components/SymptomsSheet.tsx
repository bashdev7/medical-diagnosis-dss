import { Form, Row, Col } from 'antd';
import React, { Component } from 'react';
import { reactLocalStorage } from 'reactjs-localstorage';
import { getInfoData } from './getInfoData';
import SymptomsSelect from './SymptomsSelect';
class index extends Component<any, any> {
    handleInputChange: (name: string, value: any) => void;
    constructor(props: any) {
        super(props)

        this.handleInputChange = (name: string, value: any) => {
            this.setState({ [name]: value } as any, () => reactLocalStorage.setObject('diagnostica_symptoms', this.state))
        }

        this.state = {
            symptoms: []
        }
    }


    componentDidMount() {
        getInfoData('diagnostica_symptoms').then((data: any) => {
            const { symptoms } = data
            this.setState({ symptoms })
        })
    }


    render() {
        return (
            <div className="sub-form">
                <Row gutter={[50, 16]}>
                    <Col span={12}>
                        <Form>
                            <Form.Item>
                                <h3>What symptoms have you observed in yourself?</h3>
                                <p>
                                    A symptom is any subjective evidence of disease, while a sign is any objective evidence of disease.Therefore, a symptom is a phenomenon that is experienced bythe individual affected by the disease, while a sign is a phenomenon that can be detected by someone other than the individual affected by the disease.
                    </p>
                            </Form.Item>
                            <Form.Item>
                                <SymptomsSelect value={this.state.symptoms} onChange={(value: any) => { this.handleInputChange("symptoms", value) }} />
                            </Form.Item>
                        </Form>
                    </Col>
                    <Col span={12}>
                    <img style={{ width: 'auto', height: '300px', margin: '0 auto' }} src={require('../assets/undraw_doctors_hwty.svg')} />
                    </Col>
                </Row>

            </div>
        )
    }
}

export default Form.create()(index)