import { filter } from 'lodash';
import React from 'react';
import symptomsCategory from '../config/symptoms';
import EditableDataTable from './EditableDataTable';
import { Tag } from 'antd';
const newTemplate = {
    name: 'none',
    description: 'none',
    category: 'none'
}


const SymptomsManager = () => {
    const _tableRef = React.useRef(null)
    const emptyValue = <Tag color="red">none</Tag>
    return (
        <EditableDataTable
            wrappedComponentRef={_tableRef}
            title="Symptoms"
            path="symptoms"
            newDataTemplate={newTemplate}
            columns={
                [
                    {
                        title: 'Name',
                        dataIndex: 'name',
                        width: '30%',
                        editable: true,
                        render: (text: any) => text === "none" ? emptyValue : text,
                        sortDirections: ['descend', 'ascend'],
                        sorter: (a:any, b:any) => a.name.localeCompare(b.name), 
                    },
                    {
                        title: 'Symptom Category',
                        dataIndex: 'symptom',
                        width: '20%',
                        editable: true,
                        render: (text: any) => filter(symptomsCategory, o => o.value === text)[0] && filter(symptomsCategory, o => o.value === text)[0].name || emptyValue
                    },
                    {
                        title: 'Description',
                        dataIndex: 'description',
                        editable: true,
                        render: (text: any) => text === "none" ? emptyValue : text
                    },

                ]
            }
        />
    );
}

export default SymptomsManager;