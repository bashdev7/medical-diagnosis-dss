import { Button, Form, Icon, Input, InputNumber, notification, PageHeader, Popconfirm, Select, Spin, Table, Tooltip } from 'antd';
import { FirebaseError } from 'firebase';
import { filter } from 'lodash';
import React, { Component } from 'react';
import { Resizable } from 'react-resizable';
import { compose } from 'recompose';
import { age, bmi, bp } from '../config';
import historyCategory from '../config/history';
import symptomsCategory from '../config/symptoms';
import { addNewData, deleteDoc, getCollection, updateNewData } from '../firebase/db';
import HistorySelect from './HistorySelect';
import SymptomsSelect from './SymptomsSelect';
const { Option } = Select;

const FormItem = Form.Item;
const EditableContext = React.createContext({});

const EditableRow = ({ form, index, ...props }: { form: any, index: any }) => (<EditableContext.Provider value={form}>
    <tr {...props} />
</EditableContext.Provider>);

const EditableFormRow = Form.create()(EditableRow);

const ResizeableTitle = (props: any) => {
    const { onResize, width, ...restProps } = props;

    if (!width) {
        return <th {...restProps} />;
    }

    return (
        <Resizable
            width={width}
            height={0}
            onResize={onResize}
            draggableOpts={{ enableUserSelectHack: false }}
        >
            <th {...restProps} />
        </Resizable>
    );
};
interface EditableCellProps {
    sources_store?: any,
    inputType?: any,
    editing?: any,
    dataIndex?: any,
    title?: any,
    record?: any,
    index?: any
}
class EditableCell extends Component<EditableCellProps, any> {
    constructor(props: any) {
        super(props)
    }

    getInput = () => {
        if (this.props) {
            if (this.props.inputType === 'number') {
                return <InputNumber style={{ width: '100%' }} />;
            }
            // Input for fields with validation
            if (this.props.inputType === 'special') {
                return <Input style={{ width: '100%' }} />;
            }
            if (this.props.inputType === 'symptom_category') {
                return (
                    <Select
                        showSearch
                        style={{ width: '100%' }}
                        placeholder="Select a Category"
                        optionFilterProp="children"
                    >
                        {Object.keys(symptomsCategory).map((i: any) => {
                            const { name, value, description } = symptomsCategory[i]
                            return <Option key={`_${i}`} value={value}><Tooltip title={description}>{name}</Tooltip></Option>
                        })}
                    </Select>
                )
            }

            if (this.props.inputType === 'age_identifiers') {
                return (
                    <Select
                        showSearch
                        style={{ width: '100%' }}
                        placeholder="Select a Category"
                        optionFilterProp="children"
                        mode="multiple"
                    >
                        {Object.keys(age).map((i: any) => {
                            const { name, value } = age[i]
                            return <Option key={`_${i}`} value={value}><Tooltip title={name}>{name}</Tooltip></Option>
                        })}
                    </Select>
                )
            }

            if (this.props.inputType === 'bp_identifiers') {
                return (
                    <Select
                        showSearch
                        style={{ width: '100%' }}
                        placeholder="Select a Category"
                        optionFilterProp="children"
                        mode="multiple"
                    >
                        {Object.keys(bp).map((i: any) => {
                            const { name, value } = bp[i]
                            return <Option key={`_${i}`} value={value}><Tooltip title={name}>{name}</Tooltip></Option>
                        })}
                    </Select>
                )
            }

            if (this.props.inputType === 'bmi_identifiers') {
                return (
                    <Select
                        showSearch
                        style={{ width: '100%' }}
                        placeholder="Select a Category"
                        optionFilterProp="children"
                        mode="multiple"
                    >
                        {Object.keys(bmi).map((i: any) => {
                            const { name, value } = bmi[i]
                            return <Option key={`_${i}`} value={value}><Tooltip title={name}>{name}</Tooltip></Option>
                        })}
                    </Select>
                )
            }

            if (this.props.inputType === 'history_identifiers') {
                return <HistorySelect {...this.props} />
            }

            if (this.props.inputType === 'symptoms_identifiers') {
                return <SymptomsSelect {...this.props} />
            }

            if (this.props.inputType === 'history_classification') {
                return (
                    <Select
                        mode="multiple"
                        showSearch
                        style={{ width: '100%' }}
                        placeholder="Select a Category"
                        optionFilterProp="children"
                    >
                        {Object.keys(historyCategory).map((i: any) => {
                            const { name, value } = historyCategory[i]
                            return <Option key={i} value={value}>{name}</Option>
                        })}
                    </Select>
                )
            }
        }
        return <Input />;
    };

    render() {
        const {
            editing,
            dataIndex,
            title,
            inputType,
            record,
            index,
            ...restProps
        } = this.props;
        return (
            <EditableContext.Consumer>
                {(form: any) => {
                    const { getFieldDecorator } = form;
                    return (
                        <td {...restProps}>
                            <div>
                                {editing ? (
                                    <React.Fragment>
                                        <FormItem style={{ margin: 0 }}>
                                            {getFieldDecorator(dataIndex, {
                                                rules: [{
                                                    required: true,
                                                    message: `Please Input ${title}!`,
                                                }],
                                                initialValue: record[dataIndex],
                                            })(this.getInput())}
                                        </FormItem>
                                    </React.Fragment>
                                ) : <React.Fragment>
                                        {restProps.children}
                                    </React.Fragment>}
                            </div>
                        </td>
                    );
                }}
            </EditableContext.Consumer>
        );
    }
}

interface InterfaceProps {
    path?: any;
    app?: any;
    pagination?: any;
    scroll?: any;
    columns: any
    store?: any;
    firebase?: any;
    title?: any,
    subtitle?: any;
    form?: any;
    sources_store?: any
    newDataTemplate: any,
    tableWidth?: number
}

class index extends Component<InterfaceProps, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: [],
            editingKey: '',
            drawerVisible: false,
            loading: false,
        }
    }

    showDrawer = () => {
        this.setState({
            drawerVisible: true,
        });
    };

    async getData() {
        const { path } = this.props
        const snapshot = await getCollection(path).get()
        return snapshot.docs.map(doc => Object.assign({}, { key: doc.id }, doc.data()));
    }

    componentDidMount() {
        this.setState({ loading: true })
        this.getData().then((data: any) => {
            this.setState({ data, loading: false })
        })
    }

    componentDidUpdate() {

    }

    isEditing = (record: { key: any; }) => {
        const { editingKey } = this.state
        console.log(`Editing: ${editingKey ? editingKey : '<cleared>'}`)
        return record.key === editingKey;
    }

    cancel = () => {
        this.setState({ editingKey: '' })
    };

    edit(key: any) {
        this.setState({ editingKey: key })
    }

    saveData(form: any, key: string) {
        console.log(`Saving data: ${key}`)
        const { path } = this.props
        form.validateFields((error: any, row: any) => {
            console.log("Validating form...")
            if (error) {
                console.log({ error })
                return;
            }

            const newData = [...this.state.data];
            const index = newData.findIndex(item => key === item.key);
            console.log({ index })
            if (index > -1) {
                const item = newData[index];
                newData.splice(index, 1, {
                    ...item,
                    ...row,
                });
                const toSave = filter(newData, (o: any) => o.key === key)[0]
                updateNewData(path, key, toSave).then(() => {
                    this.setState({ data: newData, editingKey: '' });
                }).catch((error: FirebaseError) => {
                    notification.error({
                        message: 'Sorry',
                        description: error.message,
                    })
                })
            } else {
                newData.push(row);
                const toSave = filter(newData, (o: any) => o.key === key)[0]
                updateNewData(path, key, toSave).then(() => {
                    this.setState({ data: newData, editingKey: '' });
                }).catch((error: FirebaseError) => {
                    notification.error({
                        message: 'Sorry',
                        description: error.message,
                    })
                })
            }
        })
        // if (form.validateFields) {
        //     form.validateFields((error: any, row: any) => {
        //         if (error) {
        //             console.log({ error })
        //             return;
        //         }
        //         const newData: any = [...this.state.data];
        //         console.log({newData})
        //         const index = newData.findIndex((item: any) => key === item.key);
        //         if (index > -1) {
        //             let item = newData[index];

        //             newData.splice(index, 1, {
        //                 ...item,
        //                 ...row,
        //             })

        //             updateNewData(path, newData[0].key, newData[0]).then(() => {
        //                 this.setState({ data: newData, editingKey: '' });
        //             }).catch((error: FirebaseError) => {
        //                 notification.error({
        //                     message: 'Sorry',
        //                     description: error.message,
        //                 })
        //             })
        //         } else {
        //             newData.push(row);
        //             updateNewData(path, newData[0].key, newData[0]).then(() => {
        //                 this.setState({ data: newData, editingKey: '' });
        //             }).catch((error: FirebaseError) => {
        //                 notification.error({
        //                     message: 'Sorry',
        //                     description: error.message,
        //                 })
        //             })
        //         }
        //     });
        // } else {
        //     notification.error({
        //         message: 'Sorry',
        //         description: "Form not validated.",
        //     })
        // }
    }

    addData = () => {
        const { data } = this.state;
        const { path, newDataTemplate } = this.props

        const transaction = addNewData(path, newDataTemplate)
        const newData = Object.assign({}, { key: transaction.id }, newDataTemplate)

        transaction.result.then(() => {
            this.setState({
                data: [newData, ...data]
            }, () => notification.info({
                message: 'Success',
                description: "Data added.",
            }))
        }).catch(() => {
            notification.error({
                message: 'Sorry',
                description: "Action could not be completed!",
            })
        })
    }


    deleteData(key: string) {
        const { path } = this.props
        //const { firebase } = this.props
        const data = [...this.state.data];
        this.setState({ data: data.filter(item => item.key !== key) }, () => deleteDoc(path, key).then(() => {
            notification.info({
                message: 'Success',
                description: "Data deleted.",
            })
        }).catch((error: FirebaseError) => {
            notification.error({
                message: 'Sorry',
                description: error.message,
            })
        }));
    }

    onDrawerClose = () => {
        this.setState({
            drawerVisible: false,
        });
    }

    handleResize(index:any){
        console.log(`Resizing... ${index}`)
    }

    render() {
        const { loading } = this.state
        const { title, subtitle, columns } = this.props

        const components = {
            body: {
                row: EditableFormRow,
                cell: EditableCell,
            },
            header: {
                cell: ResizeableTitle,
            },
        };

        const newColumns = columns.concat({
            title: 'Action',
            width: '10%',
            dataIndex: 'operation',
            render: (_text: any, record: { key: any; }) => {
                const editable = this.isEditing(record)
                return (
                    <div style={{ textAlign: 'center' }}>
                        {editable ? (
                            <span>
                                <EditableContext.Consumer>
                                    {(form: any) => (
                                        <Tooltip title="Save">
                                            <a
                                                onClick={() => this.saveData(form, record.key)}
                                                style={{ marginRight: 8 }}
                                            >
                                                <Icon type="save" />
                                            </a>
                                        </Tooltip>
                                    )}
                                </EditableContext.Consumer>
                                <Popconfirm
                                    title="Are you sure?"
                                    okText="Yes"
                                    cancelText="No"
                                    onConfirm={() => this.cancel()}
                                >
                                    <Tooltip title="Cancel">
                                        | <a> <Icon type="close-circle" /></a>
                                    </Tooltip>
                                </Popconfirm>
                            </span>
                        ) : (
                                <React.Fragment>
                                    <Tooltip title="Edit">
                                        <a onClick={() => this.edit(record.key)}><Icon type="edit" /></a>
                                    </Tooltip> | <Popconfirm
                                        title="Are you sure?"
                                        okText="Yes"
                                        cancelText="No"
                                        onConfirm={() => this.deleteData(record.key)}
                                    >
                                        <a>
                                            <Tooltip title="Delete">
                                                <Icon type="delete" />
                                            </Tooltip>
                                        </a>
                                    </Popconfirm>
                                </React.Fragment>
                            )}
                    </div>
                );
            },
        })

        const columnSet = newColumns.map((col: any, index:any) => {
            let inputType = 'text'
            if (!col.editable) {
                return col;
            }

            switch (col.dataIndex) {
                case 'symptom': inputType = 'symptom_category'
                    break;
                case 'history_classification': inputType = 'history_classification'
                    break;
                case 'history_identifiers': inputType = 'history_identifiers'
                    break;
                case 'symptoms_identifiers': inputType = 'symptoms_identifiers'
                    break;
                case 'age_identifiers': inputType = 'age_identifiers'
                    break;
                case 'bp_identifiers': inputType = 'bp_identifiers'
                    break;
                case 'bmi_identifiers': inputType = 'bmi_identifiers'
                    break;
                case 'value': inputType = 'special'
                    break;
                default: inputType = 'text'
            }

            return {
                ...col,
                onHeaderCell: (column:any) => ({
                    width: column.width,
                    onResize: this.handleResize(index)
                }),
                onCell: (record: { key: any; }) => ({
                    record,
                    inputType,
                    dataIndex: col.dataIndex,
                    title: col.title,
                    editing: this.isEditing(record),
                }),
            };
        });

        return (
            <div>
                <PageHeader title={<div style={{ maxWidth: '800px' }}>{title || ''}</div>}
                    subTitle={subtitle || ''}
                    extra={[
                        <Button key={1} type="primary" onClick={() => this.addData()}>Add</Button>
                    ]}
                >
                    <div style={{ marginTop: 20 }} >
                        <Spin spinning={loading}>
                            <Table
                                useFixedHeader={true}
                                loading={!this.state.data}
                                components={components}
                                bordered
                                dataSource={this.state.data}
                                scroll={{ x: this.props.tableWidth || 800, y: 500 }}
                                columns={columnSet}
                                pagination={{
                                    onChange: this.cancel,
                                    pageSize: 10
                                }}
                            />
                        </Spin>
                    </div>
                </PageHeader>

            </div>
        );
    }
}


export default compose<any, any>(
)(Form.create()(index))