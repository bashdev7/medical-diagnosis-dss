import { Alert, Col, Empty, List, Row, Spin, Tag } from 'antd';
import { filter, startCase, sortBy } from 'lodash';
import React, { Component } from 'react';
import { reactLocalStorage } from 'reactjs-localstorage';
import { db } from '../firebase';
import { calculateAge, calculateBMI, calculateBP, getDiagnosis, getLocalData } from '../utils';
import DiagnosisCard from './DiagnosisCard';
import { getInfoData } from './getInfoData';

class ConditionsSheet extends Component<any, any> {
    count: number;
    constructor(props: any) {
        super(props)
        this.state = {
            diagnosis_set: [],
            loading: true,
            error: '',
            user_data: null,
            history: null,
            symptoms: null
        }
        this.count = 0
    }

    getDiagnosis = () => {
        getLocalData().then((result: any) => {
            return new Promise((resolve, _reject) => {
                const history_identifiers: any = []
                const historyListing = result[1].history
                Object.keys(historyListing).map(i => {
                    if (historyListing[i]) {
                        history_identifiers.push(i)
                    }
                })

                const age_identifiers = calculateAge(result[0].age)
                const bmi_identifiers = calculateBMI(result[0].weight, result[0].height)
                const bp_identifiers = calculateBP(result[0].systolic, result[0].diastolic)
                const symptoms_identifiers = result[2].symptoms
                reactLocalStorage.setObject('diagnostica_current_user', { age_identifiers, bmi_identifiers, bp_identifiers, history_identifiers, symptoms_identifiers })
                resolve({ age_identifiers, bmi_identifiers, bp_identifiers, history_identifiers, symptoms_identifiers })
            })
        }).then((result: any) => {
            console.log({query: result})
            return Promise.all([
                getDiagnosis(result, false),
                getInfoData("diagnostica_current_user")
            ])
        }).then((result: any) => {
            const { matches } = result[0].data
            this.setState({ diagnosis_set: matches, loading: false, user_data: result[1] })
            console.log({matches})
            const profileData = {
                matches,
                profile: result[1],

            }
            reactLocalStorage.setObject('diagnostica_recent_save', profileData)
        }).catch(error => {
            this.setState({ error: error.message, loading: false })
        })
    }

    getMeta() {
        db.getCollection("history").onSnapshot(doc => {
            const historyBucket: any = []
            doc.forEach(i => {
                historyBucket.push(i.data())
            })
            this.setState({ history: historyBucket })
        })
        db.getCollection("symptoms").onSnapshot(doc => {
            const symptomsBucket: any = []
            doc.forEach(i => {
                symptomsBucket.push(i.data())
            })
            this.setState({ symptoms: symptomsBucket })
        })
    }

    componentDidMount() {
        this.getDiagnosis()
        this.getMeta()
    }

    render() {
        const { loading, diagnosis_set, error, user_data, history, symptoms } = this.state
        return (
            <div style={{ marginBottom: '2rem' }}>
                <Spin tip="Running algorithm..." spinning={loading}>
                    <div style={{ marginTop: '2rem' }}>
                        <React.Fragment>
                            <Row gutter={30}>

                                <Col span={10}>
                                    <h3>Your Profile</h3>
                                    {user_data && <List
                                        header={<div style={{ fontWeight: 'bold' }}>Patient Info</div>}
                                        footer={<div></div>}
                                        bordered
                                        dataSource={[
                                            <span>Age Classification: <Tag color="blue">{startCase(user_data.age_identifiers)}</Tag></span>,
                                            <span>BMI: <Tag color="blue">{startCase(user_data.bmi_identifiers)}</Tag></span>,
                                            <span>Blood Pressure: <Tag color="blue">{startCase(user_data.bp_identifiers)}</Tag></span>,
                                            <span>History: {
                                                <React.Fragment>
                                                    {
                                                        history ? user_data.history_identifiers.map((i: string) => <Tag color="blue">{filter(history, o => o.key === i)[0].name}</Tag>) : ""
                                                    }
                                                </React.Fragment>
                                            }</span>,
                                            <span>Symptoms: {
                                                <React.Fragment>
                                                    {
                                                        symptoms ? user_data.symptoms_identifiers.map((i: string) => <Tag color="blue">{filter(symptoms, o => o.key === i)[0].name}</Tag>) : ""
                                                    }
                                                </React.Fragment>
                                            }</span>,
                                        ]}
                                        renderItem={item => (
                                            <List.Item>
                                                {item}
                                            </List.Item>
                                        )}
                                    />}
                                </Col>
                                <Col span={14}>
                                    {user_data && <React.Fragment>
                                        <h3>Related Diseases</h3>
                                        <React.Fragment>
                                            {diagnosis_set && diagnosis_set.length >= 1 ?
                                                <React.Fragment>
                                                    {sortBy(diagnosis_set, (o:any)=>{                                                        
                                                        return o.score
                                                    }).reverse().map((i: any) => {
                                                        if(i.item.key){
                                                            return (
                                                                <DiagnosisCard score={i.score} meta={[history, symptoms]} docID={i.item.key} key={i.item.key} userData={user_data} />
                                                            )
                                                        }
                                                    })}
                                                </React.Fragment> :
                                                <Empty description="System found no match with our current database. Please try with another data." />
                                            }
                                        </React.Fragment>
                                    </React.Fragment>}
                                </Col>
                            </Row>
                        </React.Fragment>
                    </div>
                </Spin>
                {error && <Alert showIcon type="error" message={error} />}
            </div>
        );
    }
}

export default ConditionsSheet;