import { Select, Spin } from 'antd';
import React, { Component } from 'react';
import { HistoryContext } from '../contexts/';
const { Option } = Select;
class HistorySelect extends Component {
    render() {
        let props = this.props;
        let history = this.context;
        return (<React.Fragment>
            {history ?
                <Select {...props} showSearch style={{ width: '100%' }} placeholder="Select a Category" optionFilterProp="children" mode="multiple">
                    {Object.keys(history).map((i: any) => {
                        const { name, key } = history[i];
                        return <Option key={i} value={key}>{name}</Option>;
                    })}
                </Select> : <Spin spinning />}
        </React.Fragment>);
    }
}
HistorySelect.contextType = HistoryContext;

export default HistorySelect
