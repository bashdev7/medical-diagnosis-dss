import { Form, Checkbox, Tag } from 'antd';
import React, { Component } from 'react';
import { HistoryContext } from '../contexts/';
import { reactLocalStorage } from 'reactjs-localstorage';
import { getInfoData } from './getInfoData';

const HistoryLabel = (props: any): any => {
    const historySet = props.set
    if (typeof historySet === 'object') {
        const tagBucket: any = []
        historySet.map((i: any) => {
            tagBucket.push(<Tag color="blue">{i}</Tag>)
        })
        return tagBucket
    } else {
        return null
    }
}

class index extends Component<any, any> {
    handleInputChange: (name: string, value: any) => void;
    constructor(props: any) {
        super(props)
        this.state = {}

        this.handleInputChange = (name: string, value: any) => {
            this.setState({ [name]: value } as any, () => reactLocalStorage.setObject('diagnostica_history', { history: this.state }))
        }

    }

    componentDidMount() {
        getInfoData('diagnostica_history').then((data: any) => {
            const { history } = data
            this.setState(history)
        })
    }

    render() {
        const { getFieldDecorator } = this.props.form
        let history = this.context;
        return (
            <div className="sub-form">
                <Form>
                    {
                        history.map((i: any) => {
                            if (i.key) {
                                return <Form.Item key={i.key}>
                                    {getFieldDecorator(i.key, {
                                        rules: [{ required: true, message: '' }],
                                    })(
                                        <Checkbox checked={this.state[i.key]} onChange={(value: any) => {
                                            const { target } = value
                                            this.handleInputChange(target.id, target.checked)
                                        }} key={i.key}>{i.name}</Checkbox>,
                                    )}
                                    <div key={`___${i.key}`}>{i.description}</div>
                                    <p key={`__${i.key}`}>Classification: <HistoryLabel key={`_${i.key}`} set={i.history_classification} /></p>
                                </Form.Item>
                            }
                        })
                    }
                </Form>
            </div>
        )
    }
}

index.contextType = HistoryContext

export default Form.create()(index);