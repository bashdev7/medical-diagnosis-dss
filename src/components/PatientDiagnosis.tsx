import { Button, message, Steps } from 'antd';
import DiseaseHistorySheet from 'components/DiseaseHistorySheet';
import InfoSheet from 'components/InfoSheet';
import SymptomsSheet from 'components/SymptomsSheet';
import React, { Component } from 'react';
import { withAuthorization } from '../firebase/withAuthorization';
import ConditionsSheet from './ConditionsSheet';
import { db, auth } from '../firebase';
import { getInfoData } from './getInfoData';
import { navigate } from '@reach/router';

const { Step } = Steps;



const steps = [
    {
        title: 'Info',
        content: <InfoSheet />,
    },
    {
        title: 'Symptoms',
        content: <SymptomsSheet />,
    },
    {
        title: 'History',
        content: <DiseaseHistorySheet />,
    },
    {
        title: 'Prognosis',
        content: <ConditionsSheet />,
    },
];


class index extends Component<any, any> {
    constructor(props: any) {
        super(props)
        this.state = {
            current: 0
        }
    }
    next() {
        const current = this.state.current + 1;
        this.setState({ current });
    }

    prev() {
        const current = this.state.current - 1;
        this.setState({ current });
    }

    saveUserData() {
        getInfoData('diagnostica_recent_save').then(data => {            
            db.addNewProfileData(auth.getCurrentUser().uid, Object.assign({}, data, { timestamp: db.timestamp() }))
                .then(() => {
                    message.success('Processing complete! Your data has been saved.')
                    setTimeout(() => {
                        navigate('/patient/profile')
                    }, 2000)
                }).catch(error => {
                    this.setState({ error: error.message, loading: false })
                })
        })
    }

    render() {
        const { current } = this.state;
        return (
            <React.Fragment>
                <h2>Welcome to Diagnostica!</h2>
                <p> Decision Support Frame-Based Expert System for identifying possible conditions and treatment related to your symptoms.</p>
                <div className="steps">
                    <Steps current={current}>
                        {steps.map(item => (
                            <Step key={item.title} title={item.title} />
                        ))}
                    </Steps>
                    <div className="steps-content">{steps[current].content}</div>
                    <div className="steps-action">
                        {current < steps.length - 1 && (
                            <Button type="primary" onClick={() => this.next()}>
                                Next
                                    </Button>
                        )}
                        {current === steps.length - 1 && (
                            <Button type="primary" onClick={() => this.saveUserData()}>
                                Save
                                    </Button>
                        )}
                        {current > 0 && (
                            <Button style={{ marginLeft: 8 }} onClick={() => this.prev()}>
                                Previous
                                    </Button>
                        )}
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const authCondition = (authUser: any) => !!authUser;

export default withAuthorization(authCondition)(index);