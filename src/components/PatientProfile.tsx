import { navigate } from '@reach/router';
import { Avatar, Button, Divider, Empty, Icon, message, PageHeader, Spin, Tag } from 'antd';
import moment from 'moment';
import React, { Component } from 'react';
import { functionsURL } from '../config';
import { auth, db } from '../firebase';
import { getCurrentUser } from '../firebase/auth';

class index extends Component<any, { user: firebase.User, currentData: any, loading: boolean }> {
    _users: any;
    _isMounted: boolean;
    constructor(props: any) {
        super(props)
        this.state = {
            user: getCurrentUser(),
            currentData: [],
            loading: false
        }
        this._users = ()=>{}
        this._isMounted = false;
    }

    componentDidMount() {
        this._isMounted = true;
        if (this._isMounted) {
            if (auth.getCurrentUser()) {
                this.setState({ loading: true })
                this._users = db.getCollection("users").doc(auth.getCurrentUser().uid).collection('data')
                    .onSnapshot((docs: firebase.firestore.QuerySnapshot) => {
                        let dataBucket: any = []
                        docs.forEach((i: firebase.firestore.QueryDocumentSnapshot) => {
                            dataBucket.push(i.data())
                        })
                        this.setState({ currentData: dataBucket, loading: false })
                    })
            }
        }

    }

    componentWillUnmount() {
        this._isMounted = false;
        //Unsubscribe
        this._users()
    }

    render() {
        const { user, currentData, loading } = this.state
        return (

            <div>
                <h3><Avatar size={32} icon="user" /> {user && `${user.email || ''}`}</h3>
                <Spin spinning={loading}>
                    <Divider />
                    <h2>Patient's Record</h2>
                    {
                        currentData.length >= 1 ?
                            currentData.map((i: any) => {
                                // console.log(i)
                                return (
                                    <React.Fragment key={`_${i.timestamp}`}>
                                        <PageHeader
                                            className="patient-record"
                                            title={moment(i.timestamp).format('MMMM Do YYYY, h:mm:ss a')}
                                            subTitle={<Tag color="blue">Diagnostica Record</Tag>}
                                            extra={[
                                                <Button target="_blank" href={`${functionsURL}getPDFReport?t=${i.timestamp}&u=${user.uid}`} shape="circle-outline" size="large" key="1">
                                                    <Icon type="printer" />
                                                </Button>,
                                                <Button onClick={() => db.getCollection('users').doc(user.uid).collection('data').where('timestamp', '==', i.timestamp).get().then(res => {
                                                    res.forEach(item => {
                                                        item.ref.delete().then(() => {
                                                            message.info('Record deleted.')
                                                        })
                                                    })
                                                })} shape="circle-outline" size="large" key="2">
                                                    <Icon type="delete" />
                                                </Button>
                                            ]}
                                        />
                                    </React.Fragment>
                                )
                            }) :
                            <React.Fragment>
                                <div><Empty description="No diagnosis entry yet." /></div>
                                <p style={{ textAlign: 'center' }}>
                                    <Button type="primary" size="large" style={{ width: '300px' }} onClick={() => navigate('/patient/diagnosis')}>Get Started</Button>
                                </p>
                            </React.Fragment>
                    }
                </Spin>
            </div>
        );
    }
}

export default index;