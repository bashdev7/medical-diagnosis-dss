import { Alert, Tag } from 'antd';
import { filter } from 'lodash';
import React from 'react';
import { age, bmi, bp } from '../config';
import { HistoryContext, SymptomsContext } from '../contexts';
import EditableDataTable from './EditableDataTable';
const newTemplate: any = {
    name: 'none',
    description: 'none',
    history_identifiers: ['none'],
    symptoms_identifiers: ['none'],
    age_identifiers: ['none'],
    bp_identifiers: ['none'],
    bmi_identifiers: ['none'],
    treatment: 'none'
}


const DiseasesManager = () => {
    const _tableRef = React.useRef(null)
    const emptyValue = <Tag color="red">none</Tag>
    return (
        <React.Fragment>
            <EditableDataTable
                wrappedComponentRef={_tableRef}
                title="Diseases"
                path="diseases"
                tableWidth={2000}
                newDataTemplate={newTemplate}
                columns={
                    [
                        {
                            title: 'Name',
                            dataIndex: 'name',
                            width: '30%',
                            editable: true,
                            render: (text: any) => text === "none" ? emptyValue : text,
                            sortDirections: ['descend', 'ascend'],
                            sorter: (a:any, b:any) => a.name.localeCompare(b.name),                            
                        },
                        {
                            title: 'History Identifiers',
                            dataIndex: 'history_identifiers',
                            width: '20%',
                            editable: true,
                            render: (value: any) => <HistoryContext.Consumer>
                                {history => {
                                    const bucket: any = []
                                    if (history && value) {
                                        Object.keys(value).map((i: any) => {
                                            const meta = filter(history, o => {
                                                //console.log(o.key === value[i])                                            
                                                return o.key === value[i]
                                            })
                                            if (meta.length > 0) {
                                                bucket.push(<Tag key={`_${i}`} color="blue">{meta[0].name}</Tag>)
                                            }
                                        })
                                        return bucket.length === 0 ? <Tag color="red">none</Tag> : bucket
                                    } else {
                                        return emptyValue
                                    }
                                }}
                            </HistoryContext.Consumer>
                        },
                        {
                            title: 'Symptom Identifiers',
                            dataIndex: 'symptoms_identifiers',
                            editable: true,
                            width: '20%',
                            render: (value: any) => <SymptomsContext.Consumer>
                                {symptoms => {
                                    const bucket: any = []
                                    if (symptoms && value) {
                                        Object.keys(value).map((i: any) => {
                                            const meta = filter(symptoms, o => {
                                                //console.log(o.key === value[i])                                            
                                                return o.key === value[i]
                                            })
                                            if (meta.length > 0) {
                                                bucket.push(<Tag key={`_${i}`} color="blue">{meta[0].name}</Tag>)
                                            }
                                        })
                                        return bucket.length === 0 ? <Tag color="red">none</Tag> : bucket
                                    } else {
                                        return emptyValue
                                    }
                                }}
                            </SymptomsContext.Consumer>
                        },
                        {
                            title: 'Age Identifiers',
                            dataIndex: 'age_identifiers',
                            editable: true,
                            width: '20%',
                            render: (value: any) => {
                                const bucket: any = []
                                if (age && value) {
                                    Object.keys(value).map((i: any) => {
                                        const meta = filter(age, o => {
                                            //console.log(o.key === value[i])                                            
                                            return o.value === value[i]
                                        })
                                        if (meta.length > 0) {
                                            bucket.push(<Tag key={`_${i}`} color="blue">{meta[0].name}</Tag>)
                                        }
                                    })
                                    return bucket.length === 0 ? <Tag color="red">none</Tag> : bucket
                                } else {
                                    return emptyValue
                                }
                            }
                        },
                        {
                            title: 'BP Identifiers',
                            dataIndex: 'bp_identifiers',
                            editable: true,
                            width: '30%',
                            render: (value: any) => {
                                const bucket: any = []
                                if (bp && value) {
                                    Object.keys(value).map((i: any) => {
                                        const meta = filter(bp, o => {
                                            //console.log(o.key === value[i])                                            
                                            return o.value === value[i]
                                        })
                                        if (meta.length > 0) {
                                            bucket.push(<Tag key={`_${i}`} color="blue">{meta[0].name}</Tag>)
                                        }
                                    })
                                    return bucket.length === 0 ? <Tag color="red">none</Tag> : bucket
                                } else {
                                    return emptyValue
                                }
                            }
                        },
                        {
                            title: 'BMI Identifiers',
                            dataIndex: 'bmi_identifiers',
                            editable: true,
                            width: '25%',
                            render: (value: any) => {
                                const bucket: any = []
                                if (bmi && value) {
                                    Object.keys(value).map((i: any) => {
                                        const meta = filter(bmi, o => {
                                            //console.log(o.key === value[i])                                            
                                            return o.value === value[i]
                                        })
                                        if (meta.length > 0) {
                                            bucket.push(<Tag key={`_${i}`} color="blue">{meta[0].name}</Tag>)
                                        }
                                    })
                                    return bucket.length === 0 ? <Tag color="red">none</Tag> : bucket
                                } else {
                                    return emptyValue
                                }
                            }
                        },
                        {
                            title: 'Description',
                            dataIndex: 'description',
                            editable: true,
                            width: '20%',
                            render: (text: any) => text === "none" ? emptyValue : text
                        },
                        {
                            title: 'Treatment',
                            dataIndex: 'treatment',
                            editable: true,
                            width: '30%',
                            render: (text: any) => text === "none" ? emptyValue : text
                        },

                    ]
                }
            />
            <Alert showIcon type="info" message="Data is trained automatically each time the patient requests diagnosis. Training can take a long time, please be patient." />
        </React.Fragment>
    )
}

export default DiseasesManager;