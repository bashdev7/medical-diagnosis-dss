import React, { Component } from 'react';
import { getUserType } from '../firebase/db';
import { Tag } from 'antd';

class UserType extends Component<{ uid: string }, { userType: string }> {
    constructor(props: any) {
        super(props)
        this.state = {
            userType: undefined
        }
    }
    componentDidMount() {
        getUserType(this.props.uid).then((result: any) => {
            const data = result.data()
            if (data && data.type) {
                this.setState({ userType: data.type })
            }
        })
    }
    render() {
        return (
            <div>
                {this.state.userType ? <Tag color="blue">doctor</Tag> : <Tag color="blue">patient</Tag>}
            </div>
        );
    }
}

export default UserType;