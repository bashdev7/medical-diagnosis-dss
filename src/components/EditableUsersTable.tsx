import { Button, Form, Icon, Input, InputNumber, Modal, notification, PageHeader, Popconfirm, Spin, Table, Tooltip, Select } from 'antd';
import { FirebaseError } from 'firebase';
import React, { Component } from 'react';
import { compose } from 'recompose';
import { deleteUser, listAllUsers, updateNewData, addNewUser, setUserType } from '../firebase/db';
import UserType from './UserType';

const FormItem = Form.Item;
const EditableContext = React.createContext({});

const EditableRow = ({ form, index, ...props }: { form: any, index: any }) => (<EditableContext.Provider value={form}>
    <tr {...props} />
</EditableContext.Provider>);

const EditableFormRow = Form.create()(EditableRow);

interface EditableCellProps {
    sources_store?: any,
    inputType?: any,
    editing?: any,
    dataIndex?: any,
    title?: any,
    record?: any,
    index?: any
}
class EditableCell extends Component<EditableCellProps, any> {
    constructor(props: any) {
        super(props)
    }

    getInput = () => {
        if (this.props) {
            if (this.props.inputType === 'number') {
                return <InputNumber style={{ width: '100%' }} />;
            }
            // Input for fields with validation
            if (this.props.inputType === 'special') {
                return <Input style={{ width: '100%' }} />;
            }
        }
        return <Input />;
    };

    render() {
        const {
            editing,
            dataIndex,
            title,
            inputType,
            record,
            index,
            ...restProps
        } = this.props;
        return (
            <EditableContext.Consumer>
                {(form: any) => {
                    const { getFieldDecorator } = form;
                    return (
                        <td {...restProps}>
                            <div>
                                {editing ? (
                                    <React.Fragment>
                                        <FormItem style={{ margin: 0 }}>
                                            {getFieldDecorator(dataIndex, {
                                                rules: [{
                                                    required: true,
                                                    message: `Please Input ${title}!`,
                                                }],
                                                initialValue: record[dataIndex],
                                            })(this.getInput())}
                                        </FormItem>
                                    </React.Fragment>
                                ) : <React.Fragment>
                                        {restProps.children}
                                    </React.Fragment>}
                            </div>
                        </td>
                    );
                }}
            </EditableContext.Consumer>
        );
    }
}

interface InterfaceProps {
    path?: any;
    app?: any;
    pagination?: any;
    scroll?: any;
    columns: any
    store?: any;
    firebase?: any;
    title?: any,
    subtitle?: any;
    form?: any;
    sources_store?: any
    newDataTemplate: any,
    tableWidth?: number
}

class index extends Component<InterfaceProps, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: [],
            editingKey: '',
            drawerVisible: false,
            loading: false,
            visible: false, // Modal,
            type: "patient"
        }
    }

    showDrawer = () => {
        this.setState({
            drawerVisible: true,
        });
    };

    async getData() {
        return listAllUsers().then(async res => {
            const data = res.data.result
            let bucket: any = []
            console.log(data.users)
            await data.users.map(
                (item: any) => {
                    bucket.push(Object.assign({}, { key: item.uid }, item))
                }
            )
            return Promise.resolve(bucket)
        })
    }

    componentDidMount() {
        this.setState({ loading: true })
        this.getData().then((data: any) => {
            this.setState({ data, loading: false })
        })
    }

    componentDidUpdate() {

    }

    isEditing = (record: { key: any; }) => {
        const { editingKey } = this.state
        return record.key === editingKey;
    }

    cancel = () => {
        this.setState({ editingKey: '' })
    };

    edit(key: any) {
        this.setState({ editingKey: key })
    }



    saveData(form: any, key: string) {
        console.log("Saving data...")
        console.log({ form, key })
        const { path } = this.props
        if (form.validateFields) {
            form.validateFields((error: any, row: any) => {
                if (error) {
                    console.log({ error })
                    return;
                }
                const newData: any = [...this.state.data];
                const index = newData.findIndex((item: any) => key === item.key);
                if (index > -1) {
                    let item = newData[index];

                    newData.splice(index, 1, {
                        ...item,
                        ...row,
                    })

                    updateNewData(path, newData[0].key, newData[0]).then(() => {
                        this.setState({ data: newData, editingKey: '' });
                    }).catch((error: FirebaseError) => {
                        notification.error({
                            message: 'Sorry',
                            description: error.message,
                        })
                    })
                } else {
                    newData.push(row);
                    updateNewData(path, newData[0].key, newData[0]).then(() => {
                        this.setState({ data: newData, editingKey: '' });
                    }).catch((error: FirebaseError) => {
                        notification.error({
                            message: 'Sorry',
                            description: error.message,
                        })
                    })
                }
            });
        } else {
            notification.error({
                message: 'Sorry',
                description: "Form not validated.",
            })
        }
    }

    addData = () => {
        this.setState({ visible: true })
    }

    submitData = () => {
        const { email, password, data, type } = this.state
        const transaction = addNewUser(email, password, type)
        transaction.then((user: any) => {

            const userData = user.data.result
            setUserType(userData.uid, type).then(() => {
                const newData = Object.assign({}, { key: userData.uid }, { email: userData.email })

                this.setState({
                    data: [newData, ...data],
                    visible: false,
                    email: '',
                    password: ''
                }, () => notification.info({
                    message: 'Success',
                    description: "Data added.",
                })
                )
            }).catch((error) => {
                console.log(error)
                notification.error({
                    message: 'Sorry',
                    description: "Action could not be completed!",
                })
            })
        }).catch((error) => {
            console.log(error)
            notification.error({
                message: 'Sorry',
                description: "Action could not be completed!",
            })
        })
    }


    deleteData(key: string) {
        const data = [...this.state.data];
        this.setState({ data: data.filter(item => item.key !== key) }, () => deleteUser(key).then(() => {
            notification.info({
                message: 'Success',
                description: "User deleted.",
            })
        }).catch((error: any) => {
            console.log(error)
            notification.error({
                message: 'Sorry',
                description: 'There was an error removing this user.',
            })
        }));
    }

    onDrawerClose = () => {
        this.setState({
            drawerVisible: false,
        });
    }

    handleCancel = (e: any) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    handleChange = (e: any) => {
        const { name, value } = e.target
        this.setState({ [name]: value })
    }

    handleTypeCancel = (type: string) => {
        this.setState({ type })
    }

    render() {
        const { loading, email, password } = this.state
        const { title, subtitle, columns } = this.props

        const components = {
            body: {
                row: EditableFormRow,
                cell: EditableCell,
            },
        };

        const typeColumns = columns.concat({
            title: 'Type',
            width: '10%',
            dataIndex: 'operation',
            render: (_text: any, record: any) => <UserType uid={record.key} />
        })

        const newColumns = typeColumns.concat({
            title: 'Action',
            width: '10%',
            dataIndex: 'operation',
            render: (_text: any, record: { key: any; }) => {
                const editable = this.isEditing(record)
                return (
                    <div style={{ textAlign: 'center' }}>
                        {editable ? (
                            <span>
                                <EditableContext.Consumer>
                                    {(form: any) => (
                                        <Tooltip title="Save">
                                            <a
                                                onClick={() => this.saveData(form, record.key)}
                                                style={{ marginRight: 8 }}
                                            >
                                                <Icon type="save" />
                                            </a>
                                        </Tooltip>
                                    )}
                                </EditableContext.Consumer>
                                <Popconfirm
                                    title="Are you sure?"
                                    okText="Yes"
                                    cancelText="No"
                                    onConfirm={() => this.cancel()}
                                >
                                    <Tooltip title="Cancel">
                                        | <a> <Icon type="close-circle" /></a>
                                    </Tooltip>
                                </Popconfirm>
                            </span>
                        ) : (
                                <React.Fragment>
                                    <Popconfirm
                                        title="Are you sure?"
                                        okText="Yes"
                                        cancelText="No"
                                        onConfirm={() => this.deleteData(record.key)}
                                    >
                                        <a>
                                            <Tooltip title="Delete">
                                                <Icon type="delete" />
                                            </Tooltip>
                                        </a>
                                    </Popconfirm>
                                </React.Fragment>
                            )}
                    </div>
                );
            },
        })

        const columnSet = newColumns.map((col: any) => {
            let inputType = 'text'
            if (!col.editable) {
                return col;
            }

            switch (col.dataIndex) {
                case 'value': inputType = 'special'
                    break;
                default: inputType = 'text'
            }

            return {
                ...col,
                onCell: (record: { key: any; }) => ({
                    record,
                    inputType,
                    dataIndex: col.dataIndex,
                    title: col.title,
                    editing: this.isEditing(record),
                }),
            };
        });
        return (
            <div>
                <Modal
                    title="Add New Patient"
                    visible={this.state.visible}
                    onOk={this.submitData}
                    onCancel={this.handleCancel}
                    okText="Add"
                >
                    <p>Email: <Input name="email" value={email} onChange={this.handleChange} type="email" placeholder="Email" /></p>
                    <p>Password: <Input name="password" value={password} onChange={this.handleChange} type="password" placeholder="Password" /></p>
                    <p>Type:<br /><Select style={{ width: '100%' }} onChange={this.handleTypeCancel} defaultValue="patient">
                        <Select.Option value="patient">Patient</Select.Option>
                        <Select.Option value="doctor">Doctor</Select.Option>
                    </Select></p>
                </Modal>
                <PageHeader title={<div style={{ maxWidth: '800px' }}>{title || ''}</div>}
                    subTitle={subtitle || ''}
                    extra={[
                        <Button key={1} type="primary" onClick={() => this.addData()}>Add</Button>
                    ]}
                >
                    <div style={{ marginTop: 20 }} >
                        <Spin spinning={loading}>
                            <Table
                                useFixedHeader={true}
                                loading={!this.state.data}
                                components={components}
                                bordered
                                dataSource={this.state.data}
                                scroll={{ x: this.props.tableWidth || 800 }}
                                columns={columnSet}
                                pagination={{
                                    onChange: this.cancel,
                                }}
                            />
                        </Spin>
                    </div>
                </PageHeader>

            </div>
        );
    }
}


export default compose<any, any>(
)(Form.create()(index))