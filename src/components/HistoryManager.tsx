import { Tag } from 'antd';
import { filter } from 'lodash';
import React from 'react';
import historyCategory from '../config/history';
import EditableDataTable from './EditableDataTable';
const newTemplate = {
    name: 'none',
    description: 'none',
    history_classification: ['none']
}


const HistoryManager = () => {
    const _tableRef = React.useRef(null)
    const emptyValue = <Tag color="red">none</Tag>
    return (
        <EditableDataTable
            wrappedComponentRef={_tableRef}
            title="History"
            path="history"            
            newDataTemplate={newTemplate}
            columns={
                [
                    {
                        title: 'Name',
                        dataIndex: 'name',
                        width: '30%',
                        editable: true,
                        render: (text: any) => text === "none" ? emptyValue : text,
                        sortDirections: ['descend', 'ascend'],
                        sorter: (a:any, b:any) => a.name.localeCompare(b.name), 
                    },
                    {
                        title: 'Classification',
                        dataIndex: 'history_classification',
                        width: '20%',
                        editable: true,
                        //render: (text: any) => filter(historyCategory, o => o.value === text)[0] && filter(historyCategory, o => o.value === text)[0].name || emptyValue
                        render: (value: any) => {
                            const bucket: any = []
                            if (historyCategory && value) {
                                Object.keys(value).map((i: any) => {                                    
                                    const meta = filter(historyCategory, o => {
                                        //console.log(o.key === value[i])                                            
                                        return o.value === value[i]
                                    })
                                    if (meta.length > 0) {
                                        bucket.push(<Tag key={`_${i}`} color="blue">{meta[0].name}</Tag>)
                                    }
                                })                                    
                                return bucket.length===0?<Tag color="red">none</Tag>:bucket
                            } else {
                                return emptyValue
                            }
                        }
                    },
                    {                        
                        title: 'Description',
                        dataIndex: 'description',
                        editable: true,
                        render: (text: any) => text === "none" ? emptyValue : text
                    },

                ]
            }
        />
    );
}

export default HistoryManager;