import React from 'react';
import EditableUsersTable from './EditableUsersTable';
import { Tag } from 'antd';

const SymptomsManager = () => {
  const _tableRef = React.useRef(null)
  const emptyValue = <Tag color="red">none</Tag>
  return (
    <EditableUsersTable
      wrappedComponentRef={_tableRef}
      title="Users"
      path="patients"
      columns={
        [
          {
            title: 'Email',
            dataIndex: 'email',
            width: '30%',
            editable: true,
            render: (text: any) => text === "none" ? emptyValue : text,
            sortDirections: ['descend', 'ascend'],
            sorter: (a:any, b:any) => a.email.localeCompare(b.email), 
          },
        ]
      }
    />
  );
}

export default SymptomsManager;