import axios from 'axios'
import { toLower } from 'lodash'
import { functionsURL } from '../config'
import { getInfoData } from '../components/getInfoData'
import dl from 'datalib'
const calculateBP = (bps: number, bpd: number) => {
    // Blood Pressure
    let bpState = 'Normal'

    if ((bps <= 90 && bps >= 70) && (bpd <= 60 && bpd >= 40)) {
        bpState = 'Low'
    }

    if ((bps <= 120 && bps >= 91) && (bpd <= 80 && bpd >= 61)) {
        bpState = 'Normal'
    }

    if ((bps <= 140 && bps >= 121) && (bpd <= 90 && bpd >= 81)) {
        bpState = 'Pre Hypertension'
    }

    if ((bps <= 160 && bps >= 141) && (bpd <= 100 && bpd >= 90)) {
        bpState = 'High: Stage 1 Hypertension'
    }

    if ((bps >= 161) && (bpd >= 101)) {
        bpState = 'High: Stage 2 Hypertension'
    }

    return toLower(bpState)
}

const calculateAge = (age: number) => {
    let ageGroup = 'child'
    if (age >= 0 && age <= 15) {
        ageGroup = 'child'
    }
    if (age >= 16 && age <= 30) {
        ageGroup = 'young'
    }
    if (age >= 31 && age <= 59) {
        ageGroup = 'middle'
    }
    if (age > 60) {
        ageGroup = 'senior'
    }
    return ageGroup
}

const calculateBMI = (weight: number, height: number) => {
    let bmiCalc = 0
    let bmiState = 'Healthy'

    if (weight !== 0 && height !== 0) {
        bmiCalc = Math.round((weight / height ** 2) * 100) / 100
    }

    if (bmiCalc >= 25.0) {
        bmiState = 'Overweight'
    }

    if (bmiCalc >= 18.5 && bmiCalc <= 24.9) {
        bmiState = 'Healthy'
    }

    if (bmiCalc < 18.5) {
        bmiState = 'Underweight'
    }

    return toLower(bmiState);
}

const getDiagnosis = (args: any, getAll: boolean) => {
    const { age_identifiers, bmi_identifiers, bp_identifiers, history_identifiers, symptoms_identifiers } = args
    const axioQuery = `${functionsURL}getDiagnosisViaMatchScores`
    console.log({ axioQuery })
    return axios.get(axioQuery, {
        params: {
            age_identifiers,
            bmi_identifiers,
            bp_identifiers,
            history_identifiers,
            symptoms_identifiers,
            getAll
        },
    })
}

const getLocalData = () => {
    const dataStores = ['diagnostica_info', 'diagnostica_history', 'diagnostica_symptoms']
    return Promise.all([
        getInfoData(dataStores[0]),
        getInfoData(dataStores[1]),
        getInfoData(dataStores[2]),
    ])
}

const getAnalytics = async (model: string) => {
    let data: any = null
    if (model === 'diseases') {
        data = dl.json('https://us-central1-diagnostica-35f50.cloudfunctions.net/getAllDiseases')
    } else {
        data = dl.json('https://us-central1-diagnostica-35f50.cloudfunctions.net/getAllPatientRecords')
    }
    console.log("=========================")
    console.log(dl.summary(data));
    console.log("=========================");
        
    return await Promise.resolve(dl.summary(data))  
}

export { calculateAge, calculateBMI, calculateBP, getDiagnosis, getLocalData, getAnalytics }
