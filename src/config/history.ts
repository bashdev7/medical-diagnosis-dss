export default [
    {
        name: "Hereditary",
        value: "hereditary",
    },
    {
        name: "Physiological",
        value: "physiological",
    },
    {
        name: "Habitual",
        value: "habitual",
    },
]
