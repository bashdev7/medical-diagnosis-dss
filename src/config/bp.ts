export default [
    {
        name: "Stage 1 Hypertension",
        value: "stage1hypertension",
    },
    {
        name: "Stage 2 Hypertension",
        value: "stage2hypertension",
    },
    {
        name: "Pre-Hypertension",
        value: "prehypertension",
    },
    {
        name: "Low Blood",
        value: "low",
    },
    {
        name: "Normal",
        value: "normal",
    },
]