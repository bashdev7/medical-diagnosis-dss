export default [
    { name: 'Healthy', value: 'healthy' },
    { name: 'Underweight', value: 'underweight' },
    { name: 'Overweight', value: 'overweight' },
]