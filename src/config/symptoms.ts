export default [
    {
        name: "none",
        value: "none",
        description: "No classification yet."
    },
    {
        name: "Abnormal basal metabolic rate",
        value: "abbmr",
        description: "An abnormal basal metabolic rate is not necessarily indicative of disease; a number of physiological factors can alter the BMR by influencing cellular metabolic activity."
    },
    {
        name: "Alcohol intolerance",
        value: "alch",
        description: "Alcohol intolerance is due to a genetic polymorphism of the enzyme alcohol dehydrogenase, the enzyme that metabolises ingested alcohol."
    },
    {
        name: "Asynergy",
        value: "asyn",
        description: "Asynergy is defective or absent co-ordination between organs, muscles, limbs or joints, resulting in a loss in movement or speed."
    },
    {
        name: "Atony",
        value: "aton",
        description: "Is a condition in which a muscle has lost its strength. It is frequently associated with the conditions atonic seizure, atonic colon, uterine atony, gastrointestinal atony (occurs postoperatively) and choreatic atonia. "
    },
    {
        name: "Calcinosis",
        value: "calci",
        description: "Calcinosis is the formation of calcium deposits in any soft tissue."
    },
    {
        name: "Calculus",
        value: "calc",
        description: "A calculus (plural calculi), often called a stone, is a concretion of material, usually mineral salts, that forms in an organ or duct of the body."
    },
    {
        name: "Dysbiosis",
        value: "dysbio",
        description: "A term for a microbial imbalance or maladaptation on or inside the body such as an impaired microbiota. "
    },
    {
        name: "Dyssynergia",
        value: "dyssy",
        description: "Dyssynergia is any disturbance of muscular coordination, resulting in uncoordinated and abrupt movements. "
    },
    {
        name: "Esthiomene",
        value: "esthio",
        description: "Esthiomene is a medical term referring to elephantiasis of the female genitals."
    },
    {
        name: "Fibrosis",
        value: "fibro",
        description: "Fibrosis is the formation of excess fibrous connective tissue in an organ or tissue in a reparative or reactive process."
    },
    {
        name: "Fistula",
        value: "fistu",
        description: "A fistula is an abnormal connection between two hollow spaces (technically, two epithelialized surfaces), such as blood vessels, intestines, or other hollow organs."
    },
    {
        name: "Lipoatrophy",
        value: "lipoa",
        description: "Lipoatrophy is the term describing the localized loss of fat tissue."
    },
    {
        name: "Malaise",
        value: "mal",
        description: "Malaise expresses a patient's uneasiness that 'something is not right' that may need a medical examination to determine the significance. "
    },
    {
        name: "Monoplegia",
        value: "monop",
        description: "Monoplegia is paralysis of a single limb, usually an arm."
    },
    {
        name: "Paresis",
        value: "par",
        description: "A condition typified by a weakness of voluntary movement, or partial loss of voluntary movement or by impaired movement."
    },
    {
        name: "Post-exertional malaise",
        value: "pem",
        description: "Post-exertional malaise (PEM) is one of the main symptoms of myalgic encephalomyelitis/chronic fatigue syndrome."
    },
    {
        name: "Proarrhythmia",
        value: "proa",
        description: "Proarrhythmia is a new or more frequent occurrence of pre-existing arrhythmias, paradoxically precipitated by antiarrhythmic therapy."
    },
    {
        name: "Protein toxicity",
        value: "pt",
        description: "Protein toxicity is the effect of the buildup of protein metabolic waste compounds due to insufficient kidney function."
    },
    {
        name: "Ureterovaginal fistula",
        value: "uf",
        description: "A urerovaginal fistula is a result of trauma, infection, pelvic surgery, radiation treatment and therapy, malignancy, or inflammatory bowel disease."
    },
]
