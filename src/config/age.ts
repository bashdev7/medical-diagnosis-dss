export default [
    {
        name: "Young",
        value: "young",
    },
    {
        name: "Child",
        value: "child",
    },
    {
        name: "Middle Age",
        value: "middle",
    },
    {
        name: "Senior",
        value: "senior",
    },
]