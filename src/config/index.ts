import age from './age'
import bp from './bp'
import bmi from './bmi'
import history from './history'
// const functionsURL = 'http://localhost:5000/diagnostica-35f50/us-central1/'
const functionsURL = 'https://us-central1-diagnostica-35f50.cloudfunctions.net/'
export {
    age,
    bp,
    history,
    bmi,
    functionsURL
}