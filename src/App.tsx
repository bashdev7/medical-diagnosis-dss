import { Router } from '@reach/router';
import 'antd/dist/antd.css';
import React, { Component } from 'react';
import { addPrefetchExcludes, Root } from 'react-static';
import './app.css';
import * as routes from './contants/routes';
import { firebase } from "./firebase";
import { withAuthentication } from './firebase/withAuthentication';
import Doctor from './pages/doctor';
import Patient from './pages/patient';
import SignIn from './pages/signin';
import SignUp from './pages/signup';
import Home from './pages/index';

// Any routes that start with 'dynamic' will be treated as non-static routes
addPrefetchExcludes(['dynamic'])
const SignUpPage = (props: any) => <SignUp {...props} />
const SignInPage = (props: any) => <SignIn {...props} />
const PatientPage = (props: any) => <Patient {...props} />
const DoctorPage = (props: any) => <Doctor {...props} />
const HomePage = (props: any) => <Home {...props} />
class index extends Component {
  _isMounted: boolean;
  constructor(props: any) {
    super(props);

    this.state = {
      authUser: null
    };

    this._isMounted = false;
  }

  public componentDidMount() {
    this._isMounted = true;
    if (this._isMounted) {
      firebase.auth.onAuthStateChanged(authUser => {
        authUser
          ? this.setState(() => ({ authUser }))
          : this.setState(() => ({ authUser: null }));
      });
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    return (

      <Root>

        <div className="content">
          <React.Suspense fallback={<em>Loading...</em>}>
            <Router>
              <HomePage path={routes.LANDING} />
              <SignInPage path={routes.SIGN_IN} />
              <SignUpPage path={routes.SIGN_UP} />
              <PatientPage path={routes.PATIENT} />
              <DoctorPage path={routes.DOCTOR} />
            </Router>
          </React.Suspense>
        </div>

      </Root>
    );
  }
}

export default withAuthentication(index);


