export const SIGN_IN = "signin";
export const SIGN_UP = "signup";
export const LANDING = "/";
export const DOCTOR = "doctor/*";
export const PATIENT = "patient/*";