import express from 'express'
import { BayesClassifier, WordTokenizer } from 'natural'
import async from 'async'

import diseases from './config/diseases'


const app = express()

const port = process.env.PORT || 3000

const classifier = new BayesClassifier()

async.forEachOf(diseases, (value, key, callback) => {
	const { symptoms, name, agethreshold, riskfactors, bmithreshold, bpthreshold } = value
	const factors = [].concat.apply([], symptoms, agethreshold, riskfactors, bmithreshold, bpthreshold)
	classifier.addDocument(factors, name)
	callback()
}, err => {
	if(err){
		console.log(err.message)
	}else{
		classifier.train()
	}
})

app.listen(port)

app.get('/', (req, res) => {
	res.send({
		app: 'Diagnostica API',
	})
})

app.get('/diagnosis', (req, res) => {
	const symptoms = req.query.symptoms
	res.send({
		app: 'Diagnostica API',
		symptoms: symptoms,
		conditions: classifier.getClassifications(symptoms).slice(0, 4)
	})
})
